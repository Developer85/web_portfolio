$(document).ready(function() {
    console.log( "ready!" );

    if (!(typeof getURLParameter('partituras') === 'undefined'))
    	getItem(getURLParameter('partituras'), 'partitura');
    	// alert('Partitura OK');

    if (!(typeof getURLParameter('estilos') === 'undefined'))
    	getItem(getURLParameter('estilos'), 'estilo');
    	// alert('Estilo OK');

    if (!(typeof getURLParameter('instrumentos') === 'undefined'))
    	getItem(getURLParameter('instrumentos'), 'instrumento');
    	// alert('Instrumento OK');

    if (!(typeof getURLParameter('compositores') === 'undefined'))
    	getItem(getURLParameter('compositores'), 'compositor');
    	// alert('Compositor OK');
});

function getURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
			return sParameterName[1];
	}
}

function getItem(campo ,categ) {
	var data = [campo, categ];
	var jsonData = JSON.stringify(data);
	var outputData;

	$.ajax({
	    type: "POST",
	    async: false,
	    url: "controller.php",
	    data: 'action=18&data='+jsonData,
	    dataType: 'json',
	    success: function (dataSuccess) {
	        outputData = dataSuccess;
	    },
	    error: function (error) {
	    	outputData = error;
	    }
	});

	if (outputData[0] == true) {
		if (categ != 'partitura')
			showItem(outputData[1], categ);
		else
			showPartitura(outputData[1], outputData[2], outputData[3], outputData[4]);
		// alert('OK');
	}
	else
		alert("Error!!! Se ha producido un error con el servidor.");
}

function showItem(item, categ) {
	var divContent = '';

	switch (categ) {
		case 'estilo':
			// alert(item.nombre);
			divContent = new EJS({url: 'templates/detalles_estilo.ejs'}).render({ item: item });
			break;
		case 'instrumento':
			// alert(item.nombre);
			divContent = new EJS({url: 'templates/detalles_instrumento.ejs'}).render({ item: item });
			break;
		case 'compositor':
			// alert(item.nombre);
			divContent = new EJS({url: 'templates/detalles_compositor.ejs'}).render({ item: item });
			break;
	}

	$('#content').html(divContent);
}

function showPartitura(item, compositor, estilo, instrumentos) {
	var divContent = new EJS({url: 'templates/detalles_partitura.ejs'}).render(
		{ item: item, compositor: compositor, estilo: estilo, instrumentos: instrumentos });
	
	$('#content').html(divContent);
}
$(document).ready(function() {
    console.log( "ready!" );
    var categ = getURLParameter('categ');

    if (typeof categ != 'undefined')
    	// alert(categ);
    	getItems(categ);
    else
    	// alert('KO');
    	showCategorias();
});

function getURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
			return sParameterName[1];
	}
}

function showCategorias() {
	var categorias = ['partituras', 'estilos', 'instrumentos', 'compositores'];
	var divContent = new EJS({url: 'templates/categorias.ejs'}).render({ categorias: categorias });

	$('#content').html(divContent);
}

function getItems(categ) {
	var data = [categ];
	var jsonData = JSON.stringify(data);
	var outputData;

	$.ajax({
	    type: "POST",
	    async: false,
	    url: "controller.php",
	    data: 'action=17&data='+jsonData,
	    dataType: 'json',
	    success: function (dataSuccess) {
	        outputData = dataSuccess;
	    },
	    error: function (error) {
	    	outputData = error;
	    }
	});

	if (outputData[0] == true) {
		if (outputData[1].length > 0)
			showItems(outputData[1], categ);
		else
			showAviso(categ);
	}
	else
		alert("Error!!! Se ha producido un error con el servidor.");
}

function showItems(items, categ) {
	var divContent = '';

	// Mostrem el títol de la categoria
	divContent += new EJS({url: 'templates/titulo_categoria.ejs'}).render({ categ: categ });

	// Mostrem el contingut de la categoria
	switch (categ) {
		case 'partituras':
			for (var i in items) {
				divContent += new EJS({url: 'templates/item_partitura.ejs'}).render({ item: items[i], categ: categ });
			}
			break;

		default:
			for (var i in items) {
				divContent += new EJS({url: 'templates/item_otro.ejs'}).render({ item: items[i], categ: categ });
			}
			break;
	}

	$('#content').html(divContent);
}

function showAviso(categ) {
	var divContent = '';
	divContent += new EJS({url: 'templates/titulo_categoria.ejs'}).render({ categ: categ });

	var texto = '';

	switch (categ) {
		case 'partituras':
			texto = 'No hay ninguna partitura.';
			break;
		
		case 'estilos':
			texto = 'No hay ning&uacute;n estilo.';
			break;

		case 'instrumentos':
			texto = 'No hay ning&uacute;n instrumento.';
			break;

		case 'compositores':
			texto = 'No hay ning&uacute;n compositor.';
			break;
	}

	divContent += new EJS({url: 'templates/aviso.ejs'}).render({ texto: texto });
	$('#content').html(divContent);
}
$(document).ready(function() {
    console.log( "ready!" );
    
    var categ = getURLParameter('categ');
    getItems(categ);
});

function getURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
			return sParameterName[1];
	}
}

function getItems(categ) {
	var data = [categ];
	var jsonData = JSON.stringify(data);
	var outputData;

	$.ajax({
	    type: "POST",
	    async: false,
	    url: "controller.php",
	    data: 'action=17&data='+jsonData,
	    dataType: 'json',
	    success: function (dataSuccess) {
	        outputData = dataSuccess;
	    },
	    error: function (error) {
	    	outputData = error;
	    }
	});

	if (outputData[0] == true) {
		if (outputData[1].length > 0)
			showItemsTabla(outputData[1], categ);
		else
			showAviso(categ);
	}
	else
		alert("Error!!! Se ha producido un error con el servidor.");
}

function showItemsTabla(items, categ) {
	var divContent = '';

	// Mostrem el títol de la categoria
	divContent += new EJS({url: 'templates/titulo_categoria.ejs'}).render({ categ: categ });

	// Mostrem el contingut de la categoria
	switch (categ) {
		case 'partituras':
			// $gi = new GestorItems();
			// $compositores = $gi->obtenerItems('compositor', "");
			// $estilos = $gi->obtenerItems('estilo', "");
			// $usuarios = $gi->obtenerItems('usuario', "");
			// require "templates/tabla_partituras.php";
			break;
		case 'estilos':
			divContent += new EJS({url: 'templates/tabla_estilos.ejs'}).render({ items: items });
			break;
		case 'instrumentos':
			divContent += new EJS({url: 'templates/tabla_instrumentos.ejs'}).render({ items: items });
			break;
		case 'compositores':
			divContent += new EJS({url: 'templates/tabla_compositores.ejs'}).render({ items: items });
			break;
		case 'usuarios':
			divContent += new EJS({url: 'templates/tabla_usuarios.ejs'}).render({ items: items });
			break;
	}	

	$('#content').html(divContent);
}

function showAviso(categ) {
	var divContent = '';
	divContent += new EJS({url: 'templates/titulo_categoria.ejs'}).render({ categ: categ });

	var texto = '';

	switch (categ) {
		case 'partituras':
			texto = 'No hay ninguna partitura.';
			break;
		
		case 'estilos':
			texto = 'No hay ning&uacute;n estilo.';
			break;

		case 'instrumentos':
			texto = 'No hay ning&uacute;n instrumento.';
			break;

		case 'compositores':
			texto = 'No hay ning&uacute;n compositor.';
			break;
	}

	divContent += new EJS({url: 'templates/aviso.ejs'}).render({ texto: texto });
	$('#content').html(divContent);
}

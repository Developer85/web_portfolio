<tr>
	<td><input type="text" value="<?php echo $column['id'] ?>" id="id<?php echo $column['id'] ?>" disabled></td>
	<td><input type="text" value="<?php echo $column['titulo'] ?>" id="titulo<?php echo $column['id'] ?>"></td>
	<td>
		<select id="anios<?php echo $column['id'] ?>">
		<?php
			for ($i=2017; $i>=1990; $i--) {
				echo "<option value=$i ";
				if ($column['anio']==$i) echo 'selected';
				echo ">$i</option>";
			}
		?>
		</select>
	</td>
	<td>
		<select id="compositores<?php echo $column['id'] ?>">
		<?php
			foreach ($compositores as $rowComp => $colComp) {
				echo '<option value="' . $colComp['id'] . '" ';
				if ($column['id_compositor']==$colComp['id']) echo 'selected';
				echo '>' . ucwords($colComp['nombre']) . '</option>';
			}
		?>
		</select>
	</td>
	<td>
		<select id="estilos<?php echo $column['id'] ?>">
		<?php
			foreach ($estilos as $rowEstilo => $colEstilo) {
				echo '<option value="' . $colEstilo['id'] . '" ';
				if ($column['id_estilo']==$colEstilo['id']) echo 'selected';
				echo '>' . ucwords($colEstilo['nombre']) . '</option>';
			}
		?>
		</select>
	</td>
	<td><input type="text" value="<?php echo $column['pdf'] ?>" id="pdf<?php echo $column['id'] ?>" disabled></td>
	<td><input type="text" value="<?php echo $column['audio'] ?>" id="audio<?php echo $column['id'] ?>" disabled></td>
	<td>
	<?php
		if ($user != 'usr') {
			echo '<select id="usuarios' . $column['id'] . '">';
			foreach ($usuarios as $rowUsuario => $colUsuario) {
				echo '<option value="' . $colUsuario['email'] . '" ';
				if ($column['email_usuario']==$colUsuario['email']) echo 'selected';
				echo '>' . $colUsuario['email'] . '</option>';
			}
			echo '</select>';
		}
		else
			echo '<input type="text" value="' . $column['email_usuario'] . '" id="usuarios' . $column['id'] .'" disabled>';
	?>
	</td>
	<td>
		<input type="button" id="update<?php echo $column['id'] ?>" value="Actualizar" onclick="actualizarPartitura(<?php echo $column['id'] ?>, <?php echo '\'' . $_SESSION['usuario'] . '\'' ?>)">
		<input type="button" id="delete<?php echo $column['id'] ?>" value="Borrar" onclick="borrarPartitura(<?php echo $column['id'] ?>, <?php echo '\'' . $_SESSION['usuario'] . '\'' ?>)"><br>
		<span id="resultado<?php echo $column['id'] ?>"></span>
	</td>
</tr>

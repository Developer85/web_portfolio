-- Creació de BD
CREATE DATABASE tradicionalcatalana
	DEFAULT CHARACTER SET utf8
	DEFAULT COLLATE utf8_general_ci;

USE tradicionalcatalana;


-- Creació de taules
CREATE TABLE compositores (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(20) NOT NULL,
	apellidos VARCHAR(60) NOT NULL,
	direccion VARCHAR(100) NOT NULL,
	imagen VARCHAR(60) NOT NULL
);

CREATE TABLE usuarios (
	email VARCHAR(40) PRIMARY KEY,
	clave VARCHAR(20) NOT NULL,
	tipo CHAR(3) NOT NULL,
	estado TINYINT(1) NOT NULL
);

CREATE TABLE instrumentos (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(40) NOT NULL,
	tipo VARCHAR(20) NOT NULL,
	origen VARCHAR(20) NOT NULL,
	imagen VARCHAR(60) NOT NULL
);

CREATE TABLE estilos (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(40) NOT NULL,
	origen VARCHAR(20) NOT NULL,
	imagen VARCHAR(60) NOT NULL
);

CREATE TABLE partituras (
	id INT PRIMARY KEY AUTO_INCREMENT,
	titulo VARCHAR(60) NOT NULL,
	anio YEAR NOT NULL,
	id_compositor INT NOT NULL,
	id_estilo INT NOT NULL,
	pdf VARCHAR(60) NOT NULL,
	audio VARCHAR(60) NOT NULL,
	email_usuario VARCHAR(40) NOT NULL,
	FOREIGN KEY (id_compositor) REFERENCES compositores (id)
	ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (id_estilo) REFERENCES estilos (id)
	ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (email_usuario) REFERENCES usuarios (email)
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE partituras_instrumentos (
	id_partitura INT,
	id_instrumento INT,
	PRIMARY KEY (id_partitura, id_instrumento),
	FOREIGN KEY (id_partitura) REFERENCES partituras (id)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (id_instrumento) REFERENCES instrumentos (id)
	ON DELETE RESTRICT ON UPDATE CASCADE
);


-- Inserció de dades
INSERT INTO usuarios VALUES
	('admin@localhost', 'admin', 'adm', 1),
	('biel@localhost', 'qwe', 'usr', 1),
	('troy@localhost', '1234', 'usr', 1);

INSERT INTO compositores (nombre, apellidos, direccion, imagen) VALUES
	('primer compositor', 'primeros apellidos', 'primera direccion', 'composer_1.png'),
	('segundo compositor', 'segundos apellidos', 'segunda direccion', 'composer_2.png'),
	('tercer compositor', 'terceros apellidos', 'tercera direccion', 'composer_3.png');

INSERT INTO instrumentos (nombre, tipo, origen, imagen) VALUES
	('gralla', 'viento', 'europa', 'instrument_1_gralla.jpg'),
	('timbal', 'percusion', 'africa', 'instrument_2_timbal.jpg'),
	('sac de gemecs', 'viento', 'europa', 'instrument_3_sac.jpg'),
	('tarota', 'viento', 'europa', 'inst_tarota.jpg');

INSERT INTO estilos (nombre, origen, imagen) VALUES
	('rock', 'america', 'style_1_rock.png'),
	('country', 'america', 'style_2_country.png'),
	('metal', 'europa', 'style_3_metal.png');

INSERT INTO partituras (titulo, anio, id_compositor, id_estilo, pdf, audio, email_usuario) VALUES
	('ball de benasc', 2012, 1, 2, 'pdf_altres-ball_de_benasc.pdf', 'audio_altres-ball_de_benasc.mp3', 'admin@localhost'),
	('el bequetero', 2016, 2, 2, 'pdf_pasdoble-el_bequetero.pdf', 'audio_pasdoble-el_bequetero.mp3', 'biel@localhost');

INSERT INTO partituras_instrumentos VALUES
	(1, 1), (1, 2),
	(2, 1), (2, 2),(2, 3);


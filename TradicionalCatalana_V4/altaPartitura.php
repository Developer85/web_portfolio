<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tradicional Catalana - Alta de partitura</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/validaciones_ajax.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="altaItem">
	<div class="container-fluid">
		<!-- HEADER -->
		<?php
			require_once "models/GestorItems.php";
			$tipoUsuario = "guest";

			if (isset($_SESSION['usuario'])) {
				$user = new Usuario($_SESSION['usuario']);
				$gi = new GestorItems();
				$tipoUsuario = $gi->obtenerTipoUsuario($user->getEmail());

				if ($tipoUsuario == "usr")
					require_once "templates/header_logged_usr.php";
				else
					require_once "templates/header_logged_adm.php";
			}
			else
				require_once "templates/header_generic.php";
		?>

		<!-- CONTINGUT -->
		<div class="row" id="content">
			<div class="col-md-4 thumbnail datos">
				<form method="POST" onsubmit="return altaPartitura()" action="controller.php" enctype="multipart/form-data">
					<fieldset>
						<legend>Alta de Partitura</legend>
						<label>T&iacute;tulo: </label>
						<input type="text" id="titulo" name="titulo" placeholder="T&iacute;tulo" required>
						<br>
						<label>A&ntilde;o: </label>
						<select id="anios" name="anios">
							<?php
								for ($i=2017; $i>=1990; $i--)
									echo "<option value=$i>$i</option>";
							?>
						</select>
						<br>
						<label>Compositor: </label>
						<select id="compositores" name="compositores">
						<?php
							$rows = $gi->obtenerItems('compositor', "");

							foreach ($rows as $row => $column)
								echo '<option value="' . $column['id'] . '">' . ucwords($column['nombre']) . '</option>';
						?>
						</select>
						<br>
						<label>Estilo: </label>
						<select id="estilos" name="estilos">
						<?php
							$rows = $gi->obtenerItems('estilo', "");

							foreach ($rows as $row => $column)
								echo '<option value="' . $column['id'] . '">' . ucwords($column['nombre']) . '</option>';
						?>
						</select>
						<br><br>
						<label>Instrumentos: </label>
						<?php
							$rows = $gi->obtenerItems('instrumento', "");

							foreach ($rows as $row => $column)
								echo '<br><input type="checkbox" name="instrumentos[]" value="' . $column['id'] . '"/> ' . ucfirst($column['nombre']);
						?>						
						<br><br>
						<label class="lblFichero">PDF: </label>	
						<input type="file" id="pdf" name="pdf" placeholder="PDF"><br>
						<div class="clearfix"></div>
						<label class="lblFichero">Audio: </label>	
						<input type="file" id="audio" name="audio" placeholder="Audio"><br>
					</fieldset>
					<input type="submit" value="Alta">
					<div id="error"></div>
				</form>
			</div>			
		</div>

		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>		
	</div>
</body>
</html>

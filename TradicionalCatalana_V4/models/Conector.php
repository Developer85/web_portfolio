<?php
	class Conector {
		// Atributs
		protected $servername;
		protected $username;
		protected $password;
		protected $dbname;
		protected $conn;

		public function __construct($svr, $usr, $pass, $db) {
			$this->servername = $svr;
			$this->username = $usr;
			$this->password = $pass;
			$this->dbname = $db;
			$this->conn = NULL;
		}

		protected function conectar() {
			try {
			    $this->conn = new PDO("mysql:host=" . $this->servername . ";dbname=" . $this->dbname, $this->username, $this->password);
			    // set the PDO error mode to exception
			    $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch (PDOException $e) {
			    echo "Connection failed: " . $e->getMessage();
			}
		}

		protected function desconectar() {
			$this->conn = NULL;
		}
	}
?>

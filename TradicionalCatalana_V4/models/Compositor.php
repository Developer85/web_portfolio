<?php
	class Compositor {
		// Atributs
		private $id;
		private $nombre;
		private $apellidos;
		private $direccion;
		private $imagen;

		// Constructors
		public function __construct() {
	        $argv = func_get_args();

	        switch (func_num_args()) {
	        	case 1:
	                self::__construct1($argv[0]);
	                break;

	        	case 4:
	                self::__construct4($argv[0], $argv[1], $argv[2], $argv[3]);
	                break;

	            case 5:
	                self::__construct5($argv[0], $argv[1], $argv[2], $argv[3], $argv[4]);
	                break;
	        }
	    }

	    function __construct1($arg1) {
	    	$this->id = $arg1;
	    	$this->nombre = "";
	    	$this->apellidos = "";
	    	$this->direccion = "";
	    	$this->imagen = "";
	    }
	 	
	 	function __construct4($arg1, $arg2, $arg3, $arg4) {
	    	$this->id = "";
	    	$this->nombre = $arg1;
	    	$this->apellidos = $arg2;
	    	$this->direccion = $arg3;
	    	$this->imagen = $arg4;
	    }

	    function __construct5($arg1, $arg2, $arg3, $arg4, $arg5) {
	    	$this->id = $arg1;
	    	$this->nombre = $arg2;
	    	$this->apellidos = $arg3;
	    	$this->direccion = $arg4;
	    	$this->imagen = $arg5;
	    }
	    
		// Getters i Setters
		public function getId() {
			return $this->id;
		}

		public function setId($valor) {
        	$this->id = $valor;
    	}

   		public function getNombre() {
			return $this->nombre;
		}

		public function setNombre($valor) {
        	$this->nombre = $valor;
    	}

    	public function getApellidos() {
			return $this->apellidos;
		}

		public function setApellidos($valor) {
        	$this->apellidos = $valor;
    	}

   		public function getDireccion() {
			return $this->direccion;
		}

		public function setDireccion($valor) {
        	$this->direccion = $valor;
    	}

    	public function getImagen() {
			return $this->imagen;
		}

		public function setImagen($valor) {
        	$this->imagen = $valor;
    	}

    	// Mètodes
		public function __toString() {
			return "$this->id, $this->nombre, $this->apellidos, $this->direccion, $this->imagen" . "<br>";
		}
	}
?>

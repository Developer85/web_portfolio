<?php
	class Estilo {
		// Atributs
		private $id;
		private $nombre;
		private $origen;
		private $imagen;

		// Constructors
		public function __construct() {
	        $argv = func_get_args();

	        switch (func_num_args()) {
	        	case 1:
	                self::__construct1($argv[0]);
	                break;

	        	case 3:
	                self::__construct3($argv[0], $argv[1], $argv[2]);
	                break;

	            case 4:
	                self::__construct4($argv[0], $argv[1], $argv[2], $argv[3]);
	                break;
	        }
	    }
	 	
	 	function __construct1($arg1) {
	    	$this->id = $arg1;
	    	$this->nombre = "";
	    	$this->origen = "";
	    	$this->imagen = "";
	    }

	 	function __construct3($arg1, $arg2, $arg3) {
	    	$this->id = "";
	    	$this->nombre = $arg1;
	    	$this->origen = $arg2;
	    	$this->imagen = $arg3;
	    }

	    function __construct4($arg1, $arg2, $arg3, $arg4) {
	    	$this->id = $arg1;
	    	$this->nombre = $arg2;
	    	$this->origen = $arg3;
	    	$this->imagen = $arg4;
	    }
	    
		// Getters i Setters
		public function getId() {
			return $this->id;
		}

		public function setId($valor) {
        	$this->id = $valor;
    	}

   		public function getNombre() {
			return $this->nombre;
		}

		public function setNombre($valor) {
        	$this->nombre = $valor;
    	}

   		public function getOrigen() {
			return $this->origen;
		}

		public function setOrigen($valor) {
        	$this->origen = $valor;
    	}

    	public function getImagen() {
			return $this->imagen;
		}

		public function setImagen($valor) {
        	$this->imagen = $valor;
    	}

    	// Mètodes
		public function __toString() {
			return "$this->id, $this->nombre, $this->origen, $this->imagen" . "<br>";
		}
	}
?>

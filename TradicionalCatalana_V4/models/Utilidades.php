<?php
	require_once "models/GestorItems.php";

	class Utilidades {
		public function __construct() {}

		public function mostrarItems($rows, $type) {
			$texto = '';
			$item = '';

			switch ($type) {
				case 'partitura':
					$texto = 'Partituras';
					break;
				case 'estilo':
					$texto = 'Estilos';
					break;
				case 'instrumento':
					$texto = 'Instrumentos';
					break;
				case 'compositor':
					$texto = 'Compositores';
					break;
			}

			require "templates/titulo_categoria.php";

			foreach ($rows as $row => $column) {
				switch ($type) {
					case 'partitura':
						require "templates/item_partitura.php";
						break;

					case 'estilo':
						$item = 'estilo';
						require "templates/item_otro.php";
						break;

					case 'instrumento':
						$item = 'instrumento';
						require "templates/item_otro.php";
						break;

					case 'compositor':
						$item = 'compositor';
						require "templates/item_otro.php";
						break;
				}
			}
		}

		public function mostrarItem($row, $type) {
			switch ($type) {
				case 'partitura':
					$gi = new GestorItems();
					$nomCompositor = $gi->obtenerNombreItem($row->id_compositor, 'compositor');
					$nomEstilo = $gi->obtenerNombreItem($row->id_estilo, 'estilo');
					$instrumentos = $gi->obtenerInstrumentosPartitura($row->id);
					require "templates/detalles_partitura.php";
					break;

				case 'estilo':
					require "templates/detalles_estilo.php";
					break;

				case 'instrumento':
					require "templates/detalles_instrumento.php";
					break;

				case 'compositor':
					require "templates/detalles_compositor.php";
					break;
			}
		}

		public function mostrarAviso($tipo) {
			$texto = '';
			
			switch ($tipo) {
				case 'partitura':
					$texto = 'No hay ninguna partitura.';
					break;
				
				case 'estilo':
					$texto = 'No hay ning&uacute;n estilo.';
					break;

				case 'instrumento':
					$texto = 'No hay ning&uacute;n instrumento.';
					break;

				case 'compositor':
					$texto = 'No hay ning&uacute;n compositor.';
					break;
			}			
			
			require "templates/aviso.php";
		}

		public function mostrarItemsTabla($rows, $type, $user) {
			$texto = '';

			switch ($type) {
				case 'partitura':
					$texto = 'Partituras';
					break;
				case 'estilo':
					$texto = 'Estilos';
					break;
				case 'instrumento':
					$texto = 'Instrumentos';
					break;
				case 'compositor':
					$texto = 'Compositores';
					break;
				case 'usuario':
					$texto = 'Usuarios';
					break;
			}

			require "templates/titulo_categoria.php";

			switch ($type) {
				case 'partitura':
					$gi = new GestorItems();
					$compositores = $gi->obtenerItems('compositor', "");
					$estilos = $gi->obtenerItems('estilo', "");
					$usuarios = $gi->obtenerItems('usuario', "");
					require "templates/tabla_partituras.php";
					break;
				case 'estilo':
					require "templates/tabla_estilos.php";
					break;
				case 'instrumento':
					require "templates/tabla_instrumentos.php";
					break;
				case 'compositor':
					require "templates/tabla_compositores.php";
					break;
				case 'usuario':
					require "templates/tabla_usuarios.php";
					break;
			}
		}

		public function mostrarCategorias() {
			$categorias = array('partituras', 'estilos', 'instrumentos', 'compositores');

			require "templates/categorias.php";
		}
	}
?>

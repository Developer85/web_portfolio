<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tradicional Catalana - Gesti&oacute;n de &iacute;tems</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/validaciones_ajax.js"></script>
	<script type="text/javascript" src="js/gestion.js"></script>
	<script type="text/javascript" src="js/ejs_production.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="gestion">
	<div class="container-fluid">
		<!-- HEADER -->
		<?php require_once "templates/header_logged_adm.php"; ?>

		<!-- CONTINGUT -->
		<div class="row" id="content">
		<?php
			// require_once "models/GestorItems.php";
			// require_once "models/Utilidades.php";

			// $gi = new GestorItems();
			// $ut = new Utilidades();
			// $items = NULL;

			// if (isset($_POST['buscarItems'])) {
			// 		if (isset($_GET['categ'])) {
			// 			switch ($_GET['categ']) {
			// 				case 'partituras':
			// 					$items = $gi->buscarItems(trim($_POST['criterio']), 'partitura');
			// 					if (count($items) == 0)
			// 						$items = $gi->obtenerItems('partitura', "");
			// 					$ut->mostrarItemsTabla($items, 'partitura', 'adm');
			// 					break;

			// 				case 'estilos':
			// 					$items = $gi->buscarItems(trim($_POST['criterio']), 'estilo');
			// 					if (count($items) == 0)
			// 						$items = $gi->obtenerItems('estilo', "");
			// 					$ut->mostrarItemsTabla($items, 'estilo', 'adm');
			// 					break;

			// 				case 'instrumentos':
			// 					$items = $gi->buscarItems(trim($_POST['criterio']), 'instrumento');
			// 					if (count($items) == 0)
			// 						$items = $gi->obtenerItems('instrumento', "");
			// 					$ut->mostrarItemsTabla($items, 'instrumento', 'adm');
			// 					break;

			// 				case 'compositores':
			// 					$items = $gi->buscarItems(trim($_POST['criterio']), 'compositor');
			// 					if (count($items) == 0)
			// 						$items = $gi->obtenerItems('compositor', "");
			// 					$ut->mostrarItemsTabla($items, 'compositor', 'adm');
			// 					break;
			// 			}
			// 		}
			// 		else {
			// 			$ut->mostrarCategorias();
			// 		}
			// 	}
			// else {
			// 	if (isset($_GET['categ'])) {
			// 		switch ($_GET['categ']) {
			// 			case 'partituras':
			// 				// echo "<p>Gestionar Partituras</p>";
			// 				$items = $gi->obtenerItems('partitura', "");
			// 				if (count($items) > 0)
			// 					$ut->mostrarItemsTabla($items, 'partitura', 'adm');
			// 				else
			// 					$ut->mostrarAviso('partitura');
			// 				break;
			// 			case 'estilos':
			// 				// echo "<p>Gestionar Estilos</p>";
			// 				$items = $gi->obtenerItems('estilo', "");
			// 				if (count($items) > 0)
			// 					$ut->mostrarItemsTabla($items, 'estilo', 'adm');
			// 				else
			// 					$ut->mostrarAviso('estilo');
			// 				break;
			// 			case 'instrumentos':
			// 				// echo "<p>Gestionar Instrumentos</p>";
			// 				$items = $gi->obtenerItems('instrumento', "");
			// 				if (count($items) > 0)
			// 					$ut->mostrarItemsTabla($items, 'instrumento', 'adm');
			// 				else
			// 					$ut->mostrarAviso('instrumento');
			// 				break;
			// 			case 'compositores':
			// 				// echo "<p>Gestionar Compositores</p>";
			// 				$items = $gi->obtenerItems('compositor', "");
			// 				if (count($items) > 0)
			// 					$ut->mostrarItemsTabla($items, 'compositor', 'adm');
			// 				else
			// 					$ut->mostrarAviso('compositor');
			// 				break;
			// 			case 'usuarios':
			// 				// echo "<p>Gestionar Usuarios</p>";
			// 				$items = $gi->obtenerItems('usuario', $_SESSION['usuario']);
			// 				if (count($items) > 0)
			// 					$ut->mostrarItemsTabla($items, 'usuario', 'adm');
			// 				else
			// 					$ut->mostrarAviso('usuario');
			// 				break;
			// 		}
			// 	}
			// }
		?>
		</div>

		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>		
	</div>
</body>
</html>

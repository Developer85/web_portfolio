<?php
	class Usuario {
		// Atributs
		private $nombre;
		private $clave;
		private $email;
		private $tipo;

		// Constructors
		public function __construct() {
	        $argv = func_get_args();

	        switch (func_num_args()) {
	            case 1:
	                self::__construct1($argv[0]);
	                break;
	                
	            case 2:
	                self::__construct2($argv[0], $argv[1]);
	                break;

	            case 4:
	                self::__construct4($argv[0], $argv[1], $argv[2], $argv[3]);
	                break;
	        }
	    }
	 
	    function __construct1($arg1) {
	    	$this->nombre = $arg1;
	    	$this->clave = "";
	    	$this->email = "";
	    	$this->tipo = "";
	    }
	 
	    function __construct2($arg1, $arg2) {
	    	$this->nombre = $arg1;
	    	$this->clave = $arg2;
	    	$this->email = "";
	    	$this->tipo = "";
	    }

	    function __construct4($arg1, $arg2, $arg3, $arg4) {
	    	$this->nombre = $arg1;
	    	$this->clave = $arg2;
	    	$this->email = $arg3;
	    	$this->tipo = $arg4;
	    }
	    
		// Getters i Setters
		public function getNombre() {
			return $this->nombre;
		}

		public function setNombre($valor) {
        	$this->nombre = $valor;
    	}

   		public function getClave() {
			return $this->clave;
		}

		public function setClave($valor) {
        	$this->clave = $valor;
    	}

   		public function getEmail() {
			return $this->email;
		}

		public function setEmail($valor) {
        	$this->email = $valor;
    	}

   		public function getTipo() {
			return $this->tipo;
		}

		public function setTipo($valor) {
        	$this->tipo = $valor;
    	}

    	// Mètodes
		public function __toString() {
			return "$this->nombre, $this->clave, $this->email, $this->tipo" . "<br>";
		}

		public function existeUsuario() {
			$existe = false;
			$linea = NULL;
			$fitxer = fopen("data/usuaris.txt", "r");

			while (!feof($fitxer)) {
				$linea = explode(";", fgets($fitxer));

				if ($linea[0] == strtolower(trim($this->nombre))) {
					$existe = true;
					break 1;
				}
			}

			fclose($fitxer);
			return $existe;
		}

		public function agregarUsuario() {
			$fitxer = fopen("data/usuaris.txt", "a");

			fwrite($fitxer, PHP_EOL . strtolower(trim($this->nombre)) . ';' . strtolower(trim($this->clave)) . ';' . strtolower(trim($this->email)) . ';' . strtolower(trim($this->tipo)));
			fclose($fitxer);
		}

		public function validarUsuario() {
			if (!self::existeUsuario()) return -1;

			$linea = NULL;
			$fitxer = fopen("data/usuaris.txt", "r");

			while (!feof($fitxer)) {
				$linea = explode(";", fgets($fitxer));

				if ($linea[0] == strtolower(trim($this->nombre)) && $linea[1] == strtolower(trim($this->clave))) {
					fclose($fitxer);
					return 1;
				}
			}

			fclose($fitxer);
			return 0;
		}

		public function obtenerTipoUsuario() {
			$linea = NULL;
			$tipoUsuario = "";
			$fitxer = fopen("data/usuaris.txt", "r");

			while (!feof($fitxer)) {
				$linea = explode(";", fgets($fitxer));

				if ($linea[0] == strtolower(trim($this->nombre))) {
					$tipoUsuario = $linea[3];
					break 1;
				}
			}
			
			fclose($fitxer);
			return $tipoUsuario;
		}
	}
?>

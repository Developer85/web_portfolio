<?php
	class Pajaro {
		// Atributs
		private $codigo;
		private $nomCientifico;
		private $nomComun;
		private $origen;
		private $imagen;

		// Constructors
		public function __construct() {
	        $argv = func_get_args();

	        switch (func_num_args()) {
	            case 0:
	                self::__construct0();
	                break;

	            case 5:
	                self::__construct5($argv[0], $argv[1], $argv[2], $argv[3], $argv[4]);
	                break;
	        }
	    }

		public function __construct0() {
			$this->codigo = "";
			$this->nomCientifico = "";
			$this->nomComun = "";
			$this->origen = "";
			$this->imagen = "";
		}

		public function __construct5($arg1, $arg2, $arg3, $arg4, $arg5) {
			$this->codigo = $codigo;
			$this->nomCientifico = $nomCientifico;
			$this->nomComun = $nomComun;
			$this->origen = $origen;
			$this->imagen = $imagen;
		}

		// Getters i Setters
		public function getCodigo() {
			return $this->codigo;
		}

		public function setCodigo($valor) {
        	$this->codigo = $valor;
    	}

   		public function getNomCientifico() {
			return $this->nomCientifico;
		}

		public function setNomCientifico($valor) {
        	$this->nomCientifico = $valor;
    	}

   		public function getNomComun() {
			return $this->nomComun;
		}

		public function setNomComun($valor) {
        	$this->nomComun = $valor;
    	}

   		public function getOrigen() {
			return $this->origen;
		}

		public function setOrigen($valor) {
        	$this->origen = $valor;
    	}

   		public function getImagen() {
			return $this->imagen;
		}

		public function setImagen($valor) {
        	$this->imagen = $valor;
    	}

    	// Mètodes
		public function __toString() {
			return "$this->codigo, $this->nomCientifico, $this->nomComun, $this->origen, $this->imagen" . "<br>";
		}

		public function listarProductos($modo) {
			if (filesize("data/ocells.txt") == 0) {
				echo '<div class="col-md-4 thumbnail datos">';
				echo "<p>Sin Stock.</p>";
				echo '</div>';
			}
			else {
				if ($modo == "table") {
					echo '<table class="table table-bordered">';
				    echo '<thead><tr><th>C&oacute;digo</th><th>Nombre Cient&iacute;fico</th><th>Nombre Com&uacute;n</th><th>Origen</th><th>Imagen</th><th>Acciones</th></tr></thead>';
				    echo '<tbody>';
				}

				$linea = NULL;
				$fitxer = fopen("data/ocells.txt", "r");

				while (!feof($fitxer)) {
					$linea = explode(";", fgets($fitxer));

					if ($modo == "table") {
						echo '<tr><td>' . $linea[0] . '</td><td>' . $linea[1] . '</td><td>' . $linea[2] . '</td><td>' . $linea[3] . '</td><td>' . $linea[4] . '</td>';
						echo '<td><button type="button" class="btn btn-default btn-xs" name="editProducto"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>';
						echo '<button type="button" class="btn btn-default btn-xs" name="delProducto"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>';
					}
					else {
						echo '<div class="col-md-4 thumbnail item" id="item' . $linea[0] . '">';
						echo '<img src="images/' . $linea[4] . '" alt="' . $linea[4] . '">';
						echo '<a href="fichaProducto.php?producto=' . $linea[1] . '"><h2>' . ucwords($linea[2]) . '</h2></a>';
						echo '</div>';
					}
				}

				if ($modo == "table")
					echo '</tbody></table>';

				fclose($fitxer);
			}
		}

		public function obtenerTotalProductos() {
			$cont = 0;
			$linea = NULL;
			$fitxer = fopen("data/ocells.txt", "r");

			while (!feof($fitxer)) {
				$linea = fgets($fitxer);
				$cont++;
			}

			return $cont;
		}

		public function obtenerProductos($criterio) {
			$linea = NULL;
			$productos = array();
			$fitxer = fopen("data/ocells.txt", "r");			

			while (!feof($fitxer)) {
				$linea = explode(";", fgets($fitxer));

				if ($linea[1] == strtolower(trim($criterio)) || $linea[2] == strtolower(trim($criterio)) || $linea[3] == strtolower(trim($criterio)))
					array_push($productos, $linea);
			}
			
			fclose($fitxer);
			return $productos;
		}

		public function mostrarProductosEncontrados($productos) {
			if (count($productos) == 0)
				self::listarProductos("items");
			elseif (count($productos) == 1) {
				echo '<div class="col-md-4 thumbnail item resultante" id="item' . $productos[0][0] . '">';
				echo '<img src="images/' . $productos[0][4] . '" alt="' . $productos[0][4] . '">';
				echo '<h2>' . ucwords($productos[0][2]) . '</h2></a>';
				echo '<ul>';
				echo '<li>C&oacute;digo: ' . $productos[0][0] . '</li>';
				echo '<li>Nombre Cient&iacute;fico: ' . ucwords($productos[0][1]) . '</li>';
				echo '<li>Origen: ' . ucwords($productos[0][3]) . '</li>';
				echo '</ul>';
				echo '</div>';
			}
			else {
				for ($i=0; $i<count($productos); $i++) {
					echo '<div class="col-md-4 thumbnail item" id="item' . $productos[$i][0] . '">';
					echo '<img src="images/' . $productos[$i][4] . '" alt="' . $productos[$i][4] . '">';
					echo '<a href="fichaProducto.php?producto=' . $productos[$i][1] . '"><h2>' . ucwords($productos[$i][2]) . '</h2></a>';
					echo '</div>';
				}
			}
		}

		public function obtenerProducto($nCient) {
			$linea = NULL;
			$producto = NULL;
			$fitxer = fopen("data/ocells.txt", "r");			

			while (!feof($fitxer)) {
				$linea = explode(";", fgets($fitxer));

				if ($linea[1] == strtolower(trim($nCient))) {
					$producto = $linea;
					break 1;
				}
			}
			
			fclose($fitxer);
			return $producto;
		}

		public function mostrarProducto($producto, $modo) {
			echo '<div class="col-md-4 thumbnail item resultante text-right" id="item' . $producto[0] . '">';
			echo '<img src="images/' . $producto[4] . '" alt="' . $producto[4] . '">';

			if ($modo == "manipulate") {
				echo '<button type="button" class="btn btn-default btn-xs" name="editProducto"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>';
				echo '<button type="button" class="btn btn-default btn-xs" name="delProducto"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';
			}

			echo '<h2>' . ucwords($producto[2]) . '</h2></a>';
			echo '<ul>';
			echo '<li>C&oacute;digo: ' . $producto[0] . '</li>';
			echo '<li>Nombre Cient&iacute;fico: ' . ucwords($producto[1]) . '</li>';
			echo '<li>Origen: ' . ucwords($producto[3]) . '</li>';
			echo '</ul>';
		}

		public function existeProducto() {
			$existe = false;
			$linea = NULL;
			$fitxer = fopen("data/ocells.txt", "r");

			while (!feof($fitxer)) {
				$linea = explode(";", fgets($fitxer));

				if ($linea[1] == strtolower(trim($this->nomCientifico))) {
					$existe = true;
					break 1;
				}
			}

			fclose($fitxer);
			return $existe;
		}

		public function subirImagen() {
			if (move_uploaded_file($_FILES["foto"]["tmp_name"], "images/" . $this->imagen))
				return true;
			return false;
		}

		public function agregarProducto() {
			$fitxer = fopen("data/ocells.txt", "a");

			fwrite($fitxer, PHP_EOL . $this->codigo . ';' . strtolower(trim($this->nomCientifico)) . ';' . strtolower(trim($this->nomComun)) . ';' . strtolower(trim($this->origen)) . ';' . $this->imagen);
			fclose($fitxer);
		}
	}
?>

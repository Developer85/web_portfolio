<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Funny Birds - Ficha de producto</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="fichaProducto">
	<div class="container-fluid">		
		<!-- HEADER -->
		<?php
			require_once "models/Usuario.php";
			$tipoUsuario = "";

			if (isset($_SESSION['usuario'])) {
				$user = new Usuario($_SESSION['usuario']);
				$tipoUsuario = $user->obtenerTipoUsuario();

				if ($tipoUsuario == "usr")
					require_once "templates/header_logged_usr.php";
				else
					require_once "templates/header_logged_adm.php";
			}
			else
				require_once "templates/header_generic.php";
		?>
		
		<!-- CONTINGUT -->
		<div class="row" id="content">
			<?php
				require_once "models/Pajaro.php";

				$paj = new Pajaro();
				$producto = $paj->obtenerProducto($_GET['producto']);

				if ($tipoUsuario == "usr" || $tipoUsuario == "")
						$paj->mostrarProducto($producto, "view");
				else
					$paj->mostrarProducto($producto, "manipulate");
			?>
		</div>
		
		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>		
	</div>
</body>
</html>

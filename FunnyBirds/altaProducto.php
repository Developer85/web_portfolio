<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Funny Birds - Alta de producto</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="altaProducto">
	<div class="container-fluid">
		<!-- HEADER -->
		<?php
			require_once "models/Usuario.php";

			$user = new Usuario($_SESSION['usuario']);

			if ($user->obtenerTipoUsuario() == "usr")
				require_once "templates/header_logged_usr.php";
			else
				require_once "templates/header_logged_adm.php";			
		?>

		<!-- CONTINGUT -->
		<div class="row" id="content">
			<div class="col-md-4 thumbnail datos">
				<form method="POST" action="controller.php" enctype="multipart/form-data">
					<fieldset>
						<legend>Alta de Producto</legend>
						<label>Nombre Cient&iacute;fico: </label>
						<input type="text" name="nomCient" placeholder="Nombre Cient&iacute;fico" required><br>
						<label>Nombre Com&uacute;n: </label>						
						<input type="text" name="nomCom" placeholder="Nombre Com&uacute;n" required><br>
						<label>Origen: </label>						
						<input type="text" name="origen" placeholder="Origen" required><br>
						<label>Imagen: </label>	
						<input type="file" name="foto" placeholder="Imagen"><br>
					</fieldset>
					<button type="submit" name="datosProducto">Alta</button>
					<button type="reset">Reset</button>
				</form>
			</div>
		</div>

		<?php
			if (isset($_GET['success'])) {
				if ($_GET['success'] == -2)
					echo "<p>Error!!! Fichero de imagen NO seleccionado.</p>";
				elseif ($_GET['success'] == -1)
					echo "<p>Sorry, there was an error uploading your file.</p>";
				else
					echo "<p>Error!!! Ya existe el producto.</p>";
			}
		?>

		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>		
	</div>
</body>
</html>

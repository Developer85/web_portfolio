<div class="row" id="header">
	<div class="col-md-12">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navbar-header menu">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand not-active" href="#">FunnyBirds</a>
			</div>
			<div class="collapse navbar-collapse menu" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" class="not-active">Bienvenido, <?php echo ucwords($_SESSION['usuario']);?></a></li>
					<li><a href="index.php">Home</a></li>
					<li><a href="altaProducto.php">Insert Product</a></li>
					<li><a href="logout.php">Log out</a></li>
				</ul>
				<form class="navbar-form navbar-right" method="POST" action="">
					<input type="text" class="form-control" name="producto" placeholder="Search">
					<button type="submit" class="btn btn-default search" name="buscarProducto"><i class="glyphicon glyphicon-search"></i></button>
				</form>
			</div>
		</nav>
	</div>
</div>


<?php
	class GestorItems {

		// TODO: Revisar la funcionalitat
		public static function listarProductos($modo) {
			if (filesize("data/ocells.txt") == 0) {
				echo '<div class="col-md-4 thumbnail datos">';
				echo "<p>Sin Stock.</p>";
				echo '</div>';
			}
			else {
				if ($modo == "table") {
					echo '<table border=1>';
					echo '<tr><th>C&oacute;digo</th><th>Nombre Cientif&iacute;co</th><th>Nombre Com&uacute;n</th><th>Origen</th><th>Imagen</th></tr>';
				}

				$linea = NULL;
				$pajaro = NULL;
				$fitxer = fopen("data/ocells.txt", "r");

				while (!feof($fitxer)) {
					$linea = explode(";", fgets($fitxer));
					$pajaro = new Pajaro($linea[0], $linea[1], $linea[2], $linea[3], $linea[4]);

					if ($modo == "table") {
						echo '<tr><td>' . $pajaro->getCodigo() . '</td>' . '<td>' . $pajaro->getNomCientifico() . '</td>' . '<td>' . $pajaro->getNomComun() . '</td>' . 
							'<td>' . $pajaro->getOrigen() . '</td>' . '<td>' . $pajaro->getImagen() . '</td></tr>';
					}
					else {
						echo '<div class="col-md-4 thumbnail item" id="item' . $pajaro->getCodigo() . '">';
						echo '<img src="images/' . $pajaro->getImagen() . '" alt="' . $pajaro->getImagen() . '">';
						echo '<a href="fichaProducto.php?producto=' . $pajaro->getNomCientifico() . '"><h2>' . ucwords($pajaro->getNomComun()) . '</h2></a>';
						echo '</div>';
					}					
				}

				if ($modo == "table")
					echo '</table>';

				fclose($fitxer);
			}
		}


		public static function existeItem($criterio, $tipus, $fitxer) {
			$existe = false;
			$linea = NULL;
			$fitxer = fopen($fitxer, "r");

			if ($tipus == "pajaro") {
				$pajaro = NULL;				

				while (!feof($fitxer)) {
					$linea = explode(";", fgets($fitxer));
					$pajaro = new Pajaro($linea[0], $linea[1], $linea[2], $linea[3], $linea[4]);

					if ($pajaro->getNomCientifico() == strtolower(trim($criterio)) || $pajaro->getNomComun() == strtolower(trim($criterio)) || $pajaro->getOrigen() == strtolower(trim($criterio))) {
						$existe = true;
						break 1;
					}
				}
			}
			else {
				$usuario = NULL;

				while (!feof($fitxer)) {
					$linea = explode(";", fgets($fitxer));
					$usuario = new Usuario($linea[0], $linea[1], $linea[2], $linea[3]);

					if ($usuario->getNombre() == strtolower(trim($criterio))) {
						$existe = true;
						break 1;
					}
				}
			}

			fclose($fitxer);
			return $existe;
		}


		public static function obtenerTotalItems($fitxer) {
			$cont = 0;
			$linea = NULL;
			$fitxer = fopen($fitxer, "r");

			while (!feof($fitxer)) {
				$linea = fgets($fitxer);
				$cont++;
			}

			return $cont;
		}


		public static function agregarItem($item, $tipus, $fitxer) {
			$fitxer = fopen($fitxer, "a");

			if ($tipus == "pajaro")
				fwrite($fitxer, PHP_EOL . $pajaro->getCodigo() . ';' . strtolower(trim($pajaro->getNomCientifico())) . ';' . strtolower(trim($pajaro->getNomComun())) . ';' . strtolower(trim($pajaro->getOrigen())) . ';' . $pajaro->getImagen());
			else
				fwrite($fitxer, PHP_EOL . strtolower(trim($usuario->getNombre())) . ';' . strtolower(trim($usuario->getClave())) . ';' . strtolower(trim($usuario->getEmail())) . ';' . strtolower(trim($usuario->getTipo())));

			fclose($fitxer);
		}


		public static function subirImagen($imagen) {
			if (move_uploaded_file($_FILES["foto"]["tmp_name"], "images/" . $imagen))
				return true;
			return false;
		}


		public static function obtenerItems($criterio, $tipus, $fitxer) {
			$linea = NULL;
			$productos = array();
			$fitxer = fopen($fitxer, "r");

			if ($tipus == "pajaro") {
				$pajaro = NULL;

				while (!feof($fitxer)) {
					$linea = explode(";", fgets($fitxer));
					$pajaro = new Pajaro($linea[0], $linea[1], $linea[2], $linea[3], $linea[4]);

					if ($pajaro->getNomCientifico() == strtolower(trim($criterio)) || $pajaro->getNomComun() == strtolower(trim($criterio)) || $pajaro->getOrigen() == strtolower(trim($criterio)))
						array_push($productos, $pajaro);
				}
			}
			else {
				$usuario = NULL;

				while (!feof($fitxer)) {
					$linea = explode(";", fgets($fitxer));
					$usuario = new Usuario($linea[0], $linea[1], $linea[2], $linea[3]);

					if ($usuario->getNombre() == strtolower(trim($criterio)))
						array_push($productos, $usuario);
				}
			}			
			
			fclose($fitxer);
			return $productos;
		}


		public static function mostrarItems($productos, $tipus) {
			if ($tipus == "pajaro") {
				if (count($productos) == 1) {
					echo '<div class="col-md-4 thumbnail item resultante" id="item' . $productos[0]->getCodigo() . '">';
					echo '<img src="images/' . $productos[0]->getImagen() . '" alt="' . $productos[0]->getImagen() . '">';
					echo '<h2>' . ucwords($productos[0]->getNomComun()) . '</h2></a>';
					echo '<ul>';
					echo '<li>C&oacute;digo: ' . $productos[0]->getCodigo() . '</li>';
					echo '<li>Nombre Cient&iacute;fico: ' . ucwords($productos[0]->getNomCientifico()) . '</li>';
					echo '<li>Origen: ' . ucwords($productos[0]->getOrigen()) . '</li>';
					echo '</ul>';
					echo '</div>';
				}
				else {
					for ($i=0; $i<count($productos); $i++) {
						echo '<div class="col-md-4 thumbnail item" id="item' . $productos[$i]->getCodigo() . '">';
						echo '<img src="images/' . $productos[$i]->getImagen() . '" alt="' . $productos[$i]->getImagen() . '">';
						echo '<a href="fichaProducto.php?producto=' . $productos[$i]->getNomCientifico() . '"><h2>' . ucwords($productos[$i]->getNomComun()) . '</h2></a>';
						echo '</div>';
					}
				}
			}
		}


		public static function validarUsuario($user, $pass) {
			if (!self::existeItem($user, "usuario", "data/usuaris.txt")) return -1;

			$linea = NULL;
			$usuario = NULL;
			$fitxer = fopen("data/usuaris.txt", "r");

			while (!feof($fitxer)) {
				$linea = explode(";", fgets($fitxer));
				$usuario = new Usuario($linea[0], $linea[1], $linea[2], $linea[3]);

				if ($usuario->getNombre() == strtolower(trim($user)) && $usuario->getClave() == strtolower(trim($pass))) {
					fclose($fitxer);
					return 1;
				}
			}

			fclose($fitxer);
			return 0;
		}


		public static function obtenerTipoUsuario($user) {
			$linea = NULL;
			$usuario = NULL;
			$tipoUsuario = "";
			$fitxer = fopen("data/usuaris.txt", "r");

			while (!feof($fitxer)) {
				$linea = explode(";", fgets($fitxer));
				$usuario = new Usuario($linea[0], $linea[1], $linea[2], $linea[3]);

				if ($usuario->getNombre() == strtolower(trim($user))) {
					$tipoUsuario = $usuario->getTipo();
					break 1;
				}
			}
			
			fclose($fitxer);
			return $tipoUsuario;
		}


		// get_class($bar) --> retorna el tipus de classe.
	}
?>
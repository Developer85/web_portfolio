<?php
	session_start();
	require_once "models/Pajaro.php";
	require_once "models/Usuario.php";

	switch (true) {
		case isset($_POST['datosLogin']):
			login();
			break;

		case isset($_POST['datosProducto']):
			altaProducto();
			break;

		case isset($_POST['datosUsuario']):
			altaUsuario();
			break;
	}


	function login() {
		$user = new Usuario($_POST['user'], $_POST['pass']);

		if ($user->validarUsuario() == 1) {
			$_SESSION['usuario'] = $user->getNombre();
			header('Location: index.php');
		}
		elseif ($user->validarUsuario() == 0) {
			header('Location: login.php?success=0');
		}
		else {
			header('Location: login.php?success=-1');
		}
	}

	function altaProducto() {
		$paj = new Pajaro();
		$paj->setImagen($_FILES['foto']['name']);

		if ($paj->getImagen() == "")
			header('Location: altaProducto.php?success=-2');
		else {
			$paj->setNomCientifico($_POST['nomCient']);

			if ($paj->existeProducto())
				header('Location: altaProducto.php?success=0');
			else {
				if (!$paj->subirImagen())
					header('Location: altaProducto.php?success=-1');
				else {
					$paj->setCodigo($paj->obtenerTotalProductos() + 1);
					$paj->setNomComun($_POST['nomCom']);
					$paj->setOrigen($_POST['origen']);
					$paj->agregarProducto();
					// echo "<p>Producto dado de alta correctamente.</p>";
					header('Refresh: 2; url=index.php');
				}
			}
		}
	}

	function altaUsuario() {
		$user = new Usuario($_POST['user'], $_POST['pass'], $_POST['email'], "usr");
		
		if ($user->existeUsuario())
			header('Location: altaUsuario.php?success=0');
		else {
			$user->agregarUsuario();
			// echo "<p>Usuario registrado correctamente.</p>";
			header('Refresh: 2; url=index.php');
		}
	}
?>

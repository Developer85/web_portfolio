<!DOCTYPE html>
<html lang="en">
<head>
	<title>Funny Birds - Alta de usuario</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="altaUsuario">
	<div class="container-fluid">
		<!-- HEADER -->
		<?php require_once "templates/header_generic.php"; ?>

		<!-- CONTINGUT -->
		<div class="row" id="content">
			<div class="col-md-4 thumbnail datos">
				<form method="POST" action="controller.php">
					<fieldset>
						<legend>Datos de Registro</legend>
						<label>Usuario: </label>
						<input type="text" name="user" placeholder="User" required><br>
						<label>Password: </label>
						<input type="password" name="pass" placeholder="Pass" required><br>
						<label>Email: </label>
						<input type="text" name="email" placeholder="Email" required>
					</fieldset>
					<button type="submit" name="datosUsuario">Registrar</button>
					<button type="reset">Reset</button>
				</form>
			</div>
		</div>

		<?php
			if (isset($_GET['success'])) {
				if ($_GET['success'] == 0)
					echo "<p>Error!!! Ya existe el usuario.</p>";
			}
		?>

		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>		
	</div>
</body>
</html>

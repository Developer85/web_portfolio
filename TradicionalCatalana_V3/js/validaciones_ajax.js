$(document).ready(function() {
    console.log( "ready!" );
});

function entrar() {
	if ($("#email").val() == "" || $("#clave").val() == "") {
		$("#error").html("Algún campo está vacío.");
	}
	else {
		var data = [$("#email").val(), $("#clave").val()];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=1&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;	            
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="index.php";
	    else if (outputData[0] == false)
	    	$("#error").html(outputData[1]);
	    else
	      	$("#error").html("Error con el servidor.");
	}
}

function altaUsuario() {
	if ($("#email").val() == "" || $("#clave").val() == "")
		$("#error").html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
	else {
		var data = [$("#email").val(), $("#clave").val()];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=2&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;	            
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="index.php";
	    else if (outputData[0] == false)
	    	$("#error").html(outputData[1]);
	    else
	      	$("#error").html("Error con el servidor.");
	}
}

function altaEstilo() {
	if ($("#nombre").val() == "" || $("#fotoEstilo").val() == "") {
		$("#error").html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
		return false;
	}
	else if (($('#fotoEstilo').get(0).files[0].name.split('.').pop()).toLowerCase() != 'jpg' &&
		($('#fotoEstilo').get(0).files[0].name.split('.').pop()).toLowerCase() != 'png') {
		$("#error").html("Error!!! Imagen incorrecta.");
		return false;
	}
	else {
		var data = [$("#nombre").val(), $("#origenes").val(), 'style_' + $('#fotoEstilo').get(0).files[0].name];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=4&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;	            
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="index.php";
	    else if (outputData[0] == false) {
	    	$("#error").html(outputData[1]);
	    	return false;
	    }
	    else {
	      	$("#error").html("Error con el servidor.");
	      	return false;
	    }
	}
}

function altaCompositor() {
	if ($("#nombre").val() == "" || $("#apellidos").val() == "" || $("#direccion").val() == "" || $("#fotoCompositor").val() == "") {
		$("#error").html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
		return false;
	}
	else if (($('#fotoCompositor').get(0).files[0].name.split('.').pop()).toLowerCase() != 'jpg' &&
		($('#fotoCompositor').get(0).files[0].name.split('.').pop()).toLowerCase() != 'png') {
		$("#error").html("Error!!! Imagen incorrecta.");
		return false;
	}
	else {
		var data = [$("#nombre").val(), $("#apellidos").val(), $("#direccion").val(), 'comp_' + $('#fotoCompositor').get(0).files[0].name];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=6&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;	            
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="index.php";
	    else if (outputData[0] == false) {
	    	$("#error").html(outputData[1]);
	    	return false;
	    }
	    else {
	      	$("#error").html("Error con el servidor.");
	      	return false;
	    }
	}
}

function altaInstrumento() {
	if ($("#nombre").val() == "" || $("#fotoInstrumento").val() == "") {
		$("#error").html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
		return false;
	}
	else if (($('#fotoInstrumento').get(0).files[0].name.split('.').pop()).toLowerCase() != 'jpg' &&
		($('#fotoInstrumento').get(0).files[0].name.split('.').pop()).toLowerCase() != 'png') {
		$("#error").html("Error!!! Imagen incorrecta.");
		return false;
	}
	else {
		var data = [$("#nombre").val(), $("#tipos").val(), $("#origenes").val(), 'inst_' + $('#fotoInstrumento').get(0).files[0].name];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=5&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;	            
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="index.php";
	    else if (outputData[0] == false) {
	    	$("#error").html(outputData[1]);
	    	return false;
	    }
	    else {
	      	$("#error").html("Error con el servidor.");
	      	return false;
	    }
	}
}

function altaPartitura() {
	if ($("#titulo").val() == "" || $(":checkbox:checked").length == 0 || $("#pdf").val() == "" || $("#audio").val() == "") {
		$("#error").html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
		return false;
	}
	else if (($('#pdf').get(0).files[0].name.split('.').pop()).toLowerCase() != 'pdf') {
		$("#error").html("Error!!! PDF incorrecto.");
		return false;
	}
	else if (($('#audio').get(0).files[0].name.split('.').pop()).toLowerCase() != 'mp3' &&
		($('#audio').get(0).files[0].name.split('.').pop()).toLowerCase() != 'midi') {
		$("#error").html("Error!!! Audio incorrecto.");
		return false;
	}
	else {
		var instrumentos = [];

     	$(':checkbox:checked').each(function() {
       		instrumentos.push($(this).val());
     	});

     	var data = [$("#titulo").val(), $("#anios").val(), $("#compositores").val(),
			$("#estilos").val(), instrumentos, 'pdf_' + $('#pdf').get(0).files[0].name, 'audio_' + $('#audio').get(0).files[0].name];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=3&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;	            
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="index.php";
	    else if (outputData[0] == false) {
	    	$("#error").html(outputData[1]);
	    	return false;
	    }
	    else {
	      	$("#error").html("Error con el servidor.");
	      	return false;
	    }
	}
}

function bajaUsuario() {
	if ($("#clave").val() == "")
		$("#error").html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
	else {
		var data = [$("#email").val(), $("#clave").val()];
		var jsonData = JSON.stringify(data);
		var outputData;
		
		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=7&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;	            
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="index.php";
	    else if (outputData[0] == false)
	    	$("#error").html(outputData[1]);
	    else
	      	$("#error").html("Error con el servidor.");
	}
}

function actualizarEstilo(id) {
	if ($("#nombre" + id).val() == "" || $("#imagen" + id).val() == "")
		$("#resultado" + id).html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
	else {
		$("#resultado" + id).html("");

		var data = [$("#id" + id).val(), $("#nombre" + id).val(), $("#origenes" + id).val(), $("#imagen" + id).val(), 'estilo'];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=8&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;	            
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="gestion.php?categ=estilos";
	    else if (outputData[0] == false)
	    	$("#resultado" + id).html(outputData[1]);
	    else
	    	$("#resultado" + id).html("Error con el servidor.");
	}
}

function actualizarInstrumento(id) {
	if ($("#nombre" + id).val() == "" || $("#imagen" + id).val() == "")
		$("#resultado" + id).html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
	else {
		$("#resultado" + id).html("");

		var data = [$("#id" + id).val(), $("#nombre" + id).val(), $("#tipos" + id).val(), $("#origenes" + id).val(), $("#imagen" + id).val(), 'instrumento'];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=9&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;	            
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="gestion.php?categ=instrumentos";
	    else if (outputData[0] == false)
	    	$("#resultado" + id).html(outputData[1]);
	    else
	    	$("#resultado" + id).html("Error con el servidor.");
	}
}

function actualizarUsuario(id) {
	if ($("#clave" + id).val() == "")
		$("#resultado" + id).html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
	else {
		$("#resultado" + id).html("");

		var data = [$("#email" + id).val(), $("#clave" + id).val(), $("#tipos" + id).val(), $("#estados" + id).val(), 'usuario'];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=10&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="gestion.php?categ=usuarios";
	    else if (outputData[0] == false)
	    	$("#resultado" + id).html(outputData[1]);
	    else
	    	$("#resultado" + id).html("Error con el servidor.");
	}
}

function actualizarCompositor(id) {
	if ($("#nombre" + id).val() == "" || $("#apellidos" + id).val() == "" || $("#direccion" + id).val() == "" || $("#imagen" + id).val() == "")
		$("#resultado" + id).html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
	else {
		$("#resultado" + id).html("");

		var data = [$("#id" + id).val(), $("#nombre" + id).val(), $("#apellidos" + id).val(), $("#direccion" + id).val(), $("#imagen" + id).val(), 'compositor'];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=11&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="gestion.php?categ=compositores";
	    else if (outputData[0] == false)
	    	$("#resultado" + id).html(outputData[1]);
	    else
	    	$("#resultado" + id).html("Error con el servidor.");
	}
}

function actualizarPartitura(id, user) {
	if ($("#titulo" + id).val() == "" || $("#pdf" + id).val() == "" || $("#audio" + id).val() == "")
		$("#resultado" + id).html("Error!!! Alg&uacute;n campo est&aacute; vac&iacute;o.");
	else {
		$("#resultado" + id).html("");
		var data = [$("#id" + id).val(), $("#titulo" + id).val(), $("#anios" + id).val(), $("#compositores" + id).val(),
			$("#estilos" + id).val(), $("#pdf" + id).val(), $("#audio" + id).val(), $("#usuarios" + id).val()];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=12&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	if (user != 'admin@localhost')
	    		window.location.href="gestionPartituras.php";
	    	else
	    		window.location.href="gestion.php?categ=partituras";
	    else if (outputData[0] == false)
	    	$("#resultado" + id).html(outputData[1]);
	    else
	    	$("#resultado" + id).html("Error con el servidor.");
	}
}

function borrarEstilo(id) {
		$("#resultado" + id).html("");

		var data = [$("#id" + id).val(), 'estilo'];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=14&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="gestion.php?categ=estilos";
	    else
	    	$("#resultado" + id).html("El estilo no se ha podido borrar.");
}

function borrarCompositor(id) {
		$("#resultado" + id).html("");

		var data = [$("#id" + id).val(), 'compositor'];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=16&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="gestion.php?categ=compositores";
	    else
	    	$("#resultado" + id).html("El compositor no se ha podido borrar.");
}

function borrarInstrumento(id) {
		$("#resultado" + id).html("");

		var data = [$("#id" + id).val(), 'instrumento'];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=15&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	window.location.href="gestion.php?categ=instrumentos";
	    else
	    	$("#resultado" + id).html("El instrumento no se ha podido borrar.");
}

function borrarPartitura(id, user) {
		$("#resultado" + id).html("");

		var data = [$("#id" + id).val(), 'partitura'];
		var jsonData = JSON.stringify(data);
		var outputData;

		$.ajax({
	        type: "POST",
	        async: false,
	        url: "controller.php",
	        data: 'action=13&data='+jsonData,
	        dataType: 'json',
	        success: function (dataSuccess) {
	            outputData = dataSuccess;
	        },
	        error: function (error) {
	        	outputData = error;
	        }
	    });

	    if (outputData[0] == true)
	    	if (user != 'admin@localhost')
	    		window.location.href="gestionPartituras.php";
	    	else
	    		window.location.href="gestion.php?categ=partituras";
	    else
	    	$("#resultado" + id).html("La partitura no se ha podido borrar.");
}

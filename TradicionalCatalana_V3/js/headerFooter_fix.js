$(document).ready(function() {
    console.log( "ready!" );
    fixHeader();
    fixFooter();
});

function fixHeader() {
	if (window.location.href.indexOf("categ") == -1)
		$(".navbar-form").css({"pointer-events": "none", "opacity": "0.5"});
}

function fixFooter() {
	if (window.innerWidth < 1024)
		$('#footer div').css('position', 'relative');
	else {
		if ($('.item').length > 3)
			$('#footer div').css('position', 'relative');
		else
			$('#footer div').css('position', 'absolute');
	}
}


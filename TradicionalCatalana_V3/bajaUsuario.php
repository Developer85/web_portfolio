<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tradicional Catalana - Baja de usuario</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/validaciones_ajax.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="bajaUser">
	<div class="container-fluid">
		<!-- HEADER -->
		<?php require_once "templates/header_logged_usr.php"; ?>

		<!-- CONTINGUT -->
		<div class="row" id="content">
			<div class="col-md-4 thumbnail datos">
				<fieldset>
					<legend>Confirmaci&oacute;n</legend>
					<label>Email: </label>
					<input type="text" id="email" value="<?php echo $_SESSION['usuario'] ?>" disabled><br>
					<label>Clave: </label>
					<input type="password" id="clave" placeholder="Clave" required>
				</fieldset>
				<input type="button" onclick="bajaUsuario()" value="Continuar">
				<div id="error"></div>
			</div>
		</div>

		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>		
	</div>
</body>
</html>

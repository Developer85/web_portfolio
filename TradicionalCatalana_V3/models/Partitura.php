<?php
	class Partitura {
		// Atributs
		private $id;
		private $titulo;
		private $anio;
		private $idCompositor;
		private $idEstilo;
		private $pdf;
		private $audio;
		private $emailUsuario;

		// Constructors
		public function __construct() {
	        $argv = func_get_args();

	        switch (func_num_args()) {
	        	case 1:
	                self::__construct1($argv[0]);
	                break;

	            case 7:
	                self::__construct7($argv[0], $argv[1], $argv[2], $argv[3], $argv[4], $argv[5], $argv[6]);
	                break;

	            case 8:
	                self::__construct8($argv[0], $argv[1], $argv[2], $argv[3], $argv[4], $argv[5], $argv[6], $argv[7]);
	                break;
	        }
	    }
	 	
	 	function __construct1($arg1) {
			$this->id = $arg1;
	    	$this->titulo = "";
	    	$this->anio = "";
	    	$this->idCompositor = "";
	    	$this->idEstilo = "";
	    	$this->pdf = "";
	    	$this->audio = "";
	    	$this->emailUsuario = "";
	    }

	    function __construct7($arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7) {
			$this->id = "";
	    	$this->titulo = $arg1;
	    	$this->anio = $arg2;
	    	$this->idCompositor = $arg3;
	    	$this->idEstilo = $arg4;
	    	$this->pdf = $arg5;
	    	$this->audio = $arg6;
	    	$this->emailUsuario = $arg7;
	    }

	    function __construct8($arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8) {
	    	$this->id = $arg1;
	    	$this->titulo = $arg2;
	    	$this->anio = $arg3;
	    	$this->idCompositor = $arg4;
	    	$this->idEstilo = $arg5;
	    	$this->pdf = $arg6;
	    	$this->audio = $arg7;
	    	$this->emailUsuario = $arg8;
	    }
	    
		// Getters i Setters
		public function getId() {
			return $this->id;
		}

		public function setId($valor) {
        	$this->id = $valor;
    	}

    	public function getTitulo() {
			return $this->titulo;
		}

		public function setTitulo($valor) {
        	$this->titulo = $valor;
    	}

    	public function getAnio() {
			return $this->anio;
		}

		public function setAnio($valor) {
        	$this->anio = $valor;
    	}

    	public function getIdCompositor() {
			return $this->idCompositor;
		}

		public function setIdCompositor($valor) {
        	$this->idCompositor = $valor;
    	}

    	public function getIdEstilo() {
			return $this->idEstilo;
		}

		public function setIdEstilo($valor) {
        	$this->idEstilo = $valor;
    	}

    	public function getPdf() {
			return $this->pdf;
		}

		public function setPdf($valor) {
        	$this->pdf = $valor;
    	}

   		public function getAudio() {
			return $this->audio;
		}

		public function setAudio($valor) {
        	$this->audio = $valor;
    	}

    	public function getEmailUsuario() {
			return $this->emailUsuario;
		}

		public function setEmailUsuario($valor) {
        	$this->emailUsuario = $valor;
    	}

    	// Mètodes
		public function __toString() {
			return "$this->id, $this->titulo, $this->anio, $this->idCompositor, $this->idEstilo, $this->pdf, $this->audio, $this->emailUsuario" . "<br>";
		}
	}
?>

<?php
	class Usuario {
		// Atributs
		private $email;
		private $clave;
		private $tipo;
		private $estado;

		// Constructors
		public function __construct() {
	        $argv = func_get_args();

	        switch (func_num_args()) {
	        	case 1:
	                self::__construct1($argv[0]);
	                break;

	            case 2:
	                self::__construct2($argv[0], $argv[1]);
	                break;

	            case 4:
	                self::__construct4($argv[0], $argv[1], $argv[2], $argv[3]);
	                break;
	        }
	    }

	    function __construct1($arg1) {
	    	$this->email = $arg1;
	    	$this->clave = "";
	    	$this->tipo = "";
	    	$this->estado = "";
	    }
	 
	    function __construct2($arg1, $arg2) {
	    	$this->email = $arg1;
	    	$this->clave = $arg2;
	    	$this->tipo = "";
	    	$this->estado = "";
	    }

	    function __construct4($arg1, $arg2, $arg3, $arg4) {
	    	$this->email = $arg1;
	    	$this->clave = $arg2;
	    	$this->tipo = $arg3;
	    	$this->estado = $arg4;
	    }
	    
		// Getters i Setters
		public function getEmail() {
			return $this->email;
		}

		public function setEmail($valor) {
        	$this->email = $valor;
    	}

   		public function getClave() {
			return $this->clave;
		}

		public function setClave($valor) {
        	$this->clave = $valor;
    	}

   		public function getTipo() {
			return $this->tipo;
		}

		public function setTipo($valor) {
        	$this->tipo = $valor;
    	}

    	public function getEstado() {
			return $this->estado;
		}

		public function setEstado($valor) {
        	$this->estado = $valor;
    	}

    	// Mètodes
		public function __toString() {
			return "$this->email, $this->clave, $this->tipo, $this->estado" . "<br>";
		}
	}
?>

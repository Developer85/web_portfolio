<?php
	require_once "Conector.php";
	require_once "models/GestorItems.php";
	require_once "models/Usuario.php";
	require_once "models/Partitura.php";
	require_once "models/Estilo.php";
	require_once "models/Instrumento.php";
	require_once "models/Compositor.php";

	class GestorItems extends Conector {
		// Atributs
		
		// Constructors
		public function __construct() {
			parent::__construct('localhost', 'root', '', 'tradicionalcatalana');
	    }

	    // Mètodes Comuns
	    public function existeItem($item, $tipo) {
			try {
				$this->conectar();
				$stmt = NULL;

				switch ($tipo) {
					case 'usuario':
						$estado = 1;
						$stmt = $this->conn->prepare("SELECT * FROM usuarios WHERE email LIKE ? AND estado = ?");
						$stmt->bindParam(1, $item->getEmail());
						$stmt->bindParam(2, $estado);
						break;
					case 'partitura':
						$stmt = $this->conn->prepare("SELECT * FROM partituras WHERE titulo LIKE ?");
						$stmt->bindParam(1, $item->getTitulo());
						break;
					case 'estilo':
						$stmt = $this->conn->prepare("SELECT * FROM estilos WHERE nombre LIKE ?");
						$stmt->bindParam(1, $item->getNombre());
						break;
					case 'instrumento':
						$stmt = $this->conn->prepare("SELECT * FROM instrumentos WHERE nombre LIKE ?");
						$stmt->bindParam(1, $item->getNombre());
						break;
					case 'compositor':
						$stmt = $this->conn->prepare("SELECT * FROM compositores WHERE nombre LIKE ?");
						$stmt->bindParam(1, $item->getNombre());
						break;
				}

				$stmt->execute();
				$row = $stmt->fetchObject();
				
				if (!$row) return false;
				return true;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function agregarItem($item, $tipo) {
			try {
				$this->conectar();
				$stmt = NULL;

				switch ($tipo) {
					case 'usuario':
						$stmt = $this->conn->prepare("INSERT INTO usuarios (email, clave, tipo, estado) VALUES (?, ?, ?, ?)");
						$stmt->bindParam(1, $item->getEmail());
						$stmt->bindParam(2, $item->getClave());
						$stmt->bindParam(3, $item->getTipo());
						$stmt->bindParam(4, $item->getEstado());
						break;
					case 'estilo':
						$stmt = $this->conn->prepare("INSERT INTO estilos (nombre, origen, imagen) VALUES (?, ?, ?)");
						$stmt->bindParam(1, $item->getNombre());
						$stmt->bindParam(2, $item->getOrigen());
						$stmt->bindParam(3, $item->getImagen());
						break;
					case 'instrumento':
						$stmt = $this->conn->prepare("INSERT INTO instrumentos (nombre, tipo, origen, imagen) VALUES (?, ?, ?, ?)");
						$stmt->bindParam(1, $item->getNombre());
						$stmt->bindParam(2, $item->getTipo());
						$stmt->bindParam(3, $item->getOrigen());
						$stmt->bindParam(4, $item->getImagen());
						break;
					case 'compositor':
						$stmt = $this->conn->prepare("INSERT INTO compositores (nombre, apellidos, direccion, imagen) VALUES (?, ?, ?, ?)");
						$stmt->bindParam(1, $item->getNombre());
						$stmt->bindParam(2, $item->getApellidos());
						$stmt->bindParam(3, $item->getDireccion());
						$stmt->bindParam(4, $item->getImagen());
						break;
				}

				return $stmt->execute();
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function agregarPartitura($item, $instrumentos) {
			try {
				$this->conectar();
				$stmt = NULL;
				
				$stmt = $this->conn->prepare("INSERT INTO partituras (titulo, anio, id_compositor, id_estilo, pdf, audio, email_usuario) VALUES (?, ?, ?, ?, ?, ?, ?)");
				$stmt->bindParam(1, $item->getTitulo());
				$stmt->bindParam(2, $item->getAnio());
				$stmt->bindParam(3, $item->getIdCompositor());
				$stmt->bindParam(4, $item->getIdEstilo());
				$stmt->bindParam(5, $item->getPdf());
				$stmt->bindParam(6, $item->getAudio());
				$stmt->bindParam(7, $item->getEmailUsuario());
				
				if (!$stmt->execute()) return false;

				$this->conectar();
				$item->setId($this->obtenerIdPartitura($item->getTitulo()));

				foreach ($instrumentos as $inst) {
					$this->conectar();
					$stmt = $this->conn->prepare("INSERT INTO partituras_instrumentos VALUES (?, ?)");
					$stmt->bindParam(1, $item->getId());
					$stmt->bindParam(2, $inst);

					if (!$stmt->execute()) return false;
				}
				
				return true;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function obtenerIdPartitura($titulo) {
			try {
				$this->conectar();
				$stmt = $this->conn->prepare("SELECT id FROM partituras WHERE titulo LIKE ?");
				$stmt->bindParam(1, $titulo);
				$stmt->execute();
				$row = $stmt->fetchObject();
				
				return $row->id;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function obtenerInstrumentosPartitura($idPartitura) {
			try {
				$this->conectar();
				$stmt = $this->conn->prepare("
					SELECT i.nombre AS 'instrumento' FROM instrumentos i
					JOIN partituras_instrumentos pi ON i.id = pi.id_instrumento
					JOIN partituras p ON p.id = pi.id_partitura
					WHERE p.id = ?");
				$stmt->bindParam(1, $idPartitura);
				$stmt->execute();
				$rows = $stmt->fetchAll();

				return $rows;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function asignarUsuarioPartituras($mail) {
			try {
				$this->conectar();
				$stmt = $this->conn->prepare("UPDATE partituras SET email_usuario = ? WHERE email_usuario LIKE ?");
				$newMail = 'admin@localhost';
				$stmt->bindParam(1, $newMail);
				$stmt->bindParam(2, $mail);

				return $stmt->execute();
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function obtenerNombreItem($id, $tipo) {
			try {
				$this->conectar();

				switch ($tipo) {
					case 'compositor':
						$stmt = $this->conn->prepare("SELECT nombre FROM compositores WHERE id = ?");
						break;

					case 'estilo':
						$stmt = $this->conn->prepare("SELECT nombre FROM estilos WHERE id = ?");
						break;
				}
				
				$stmt->bindParam(1, $id);
				$stmt->execute();
				$row = $stmt->fetchObject();
				
				return $row->nombre;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function obtenerItems($tipo, $email) {
			try {
				$this->conectar();
				$stmt = NULL;

				switch ($tipo) {
					case 'usuario':
						$stmt = $this->conn->prepare("SELECT * FROM usuarios WHERE email NOT LIKE ?");
						$stmt->bindParam(1, $email);
						break;

					case 'partitura':
						if ($email == "" || $email == "admin@localhost")
							$stmt = $this->conn->prepare("SELECT * FROM partituras");
						else {
							$stmt = $this->conn->prepare("SELECT * FROM partituras WHERE email_usuario LIKE ?");
							$stmt->bindParam(1, $email);
						}
						break;

					case 'estilo':
						$stmt = $this->conn->prepare("SELECT * FROM estilos");
						break;

					case 'instrumento':
						$stmt = $this->conn->prepare("SELECT * FROM instrumentos");
						break;

					case 'compositor':
						$stmt = $this->conn->prepare("SELECT * FROM compositores");
						break;
				}				
				
				$stmt->execute();
				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

				return $rows;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function obtenerItem($nom, $tipo) {
			try {
				$this->conectar();
				$stmt = NULL;

				switch ($tipo) {
					case 'partitura':
						$stmt = $this->conn->prepare("SELECT * FROM partituras WHERE titulo LIKE ?");
						break;

					case 'estilo':
						$stmt = $this->conn->prepare("SELECT * FROM estilos WHERE nombre LIKE ?");
						break;

					case 'instrumento':
						$stmt = $this->conn->prepare("SELECT * FROM instrumentos WHERE nombre LIKE ?");
						break;

					case 'compositor':
						$stmt = $this->conn->prepare("SELECT * FROM compositores WHERE nombre LIKE ?");
						break;
				}

				$stmt->bindParam(1, $nom);
				$stmt->execute();
				$row = $stmt->fetchObject();

				return $row;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function obtenerItemById($id, $tipo) {
			try {
				$this->conectar();
				$stmt = NULL;

				switch ($tipo) {
					case 'partitura':
						$stmt = $this->conn->prepare("SELECT * FROM partituras WHERE id = ?");
						break;

					case 'estilo':
						$stmt = $this->conn->prepare("SELECT * FROM estilos WHERE id = ?");
						break;

					case 'instrumento':
						$stmt = $this->conn->prepare("SELECT * FROM instrumentos WHERE id = ?");
						break;

					case 'compositor':
						$stmt = $this->conn->prepare("SELECT * FROM compositores WHERE id = ?");
						break;
				}

				$stmt->bindParam(1, $id);
				$stmt->execute();
				$row = $stmt->fetchObject();

				return $row;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function buscarItems($criterio, $tipo) {
			try {
				$this->conectar();
				$stmt = NULL;

				switch ($tipo) {
					case 'partitura': // titulo, anio
						$stmt = $this->conn->prepare("SELECT * FROM partituras WHERE titulo LIKE ? OR anio = ?");
						$stmt->bindParam(1, $criterio);
						$stmt->bindParam(2, $criterio);
						break;

					case 'estilo': // nombre, origen
						$stmt = $this->conn->prepare("SELECT * FROM estilos WHERE nombre LIKE ? OR origen LIKE ?");
						$stmt->bindParam(1, $criterio);
						$stmt->bindParam(2, $criterio);
						break;

					case 'instrumento': // nombre, tipo, origen
						$stmt = $this->conn->prepare("SELECT * FROM instrumentos WHERE nombre LIKE ? 
							OR tipo LIKE ? OR origen LIKE ?");
						$stmt->bindParam(1, $criterio);
						$stmt->bindParam(2, $criterio);
						$stmt->bindParam(3, $criterio);
						break;

					case 'compositor': // nombre, apellidos, direccion
						$stmt = $this->conn->prepare("SELECT * FROM compositores WHERE nombre LIKE ? 
							OR apellidos LIKE ? OR direccion LIKE ?");
						$stmt->bindParam(1, $criterio);
						$stmt->bindParam(2, $criterio);
						$stmt->bindParam(3, $criterio);
						break;
				}

				$stmt->execute();
				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

				return $rows;				
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function borrarItem($item, $tipo) {
			try {
				$this->conectar();
				$stmt = NULL;

				switch ($tipo) {
					case 'usuario':
						$stmt = $this->conn->prepare("UPDATE usuarios SET estado = ? WHERE email LIKE ?");
						$stmt->bindParam(1, $item->getEstado());
						$stmt->bindParam(2, $item->getEmail());
						break;

					case 'estilo':
						$stmt = $this->conn->prepare("DELETE FROM estilos WHERE id = ?");
						$stmt->bindParam(1, $item->getId());
						break;

					case 'instrumento':
						$stmt = $this->conn->prepare("DELETE FROM instrumentos WHERE id = ?");
						$stmt->bindParam(1, $item->getId());
						break;

					case 'compositor':
						$stmt = $this->conn->prepare("DELETE FROM compositores WHERE id = ?");
						$stmt->bindParam(1, $item->getId());
						break;
				}

				return $stmt->execute();
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function borrarPartitura($id) {
			try {
				$this->conectar();
				$stmt = NULL;

				$stmt = $this->conn->prepare("DELETE FROM partituras_instrumentos WHERE id_partitura = ?");
				$stmt->bindParam(1, $id);

				if (!$stmt->execute()) return false;

				$this->conectar();
				$stmt = $this->conn->prepare("DELETE FROM partituras WHERE id = ?");
				$stmt->bindParam(1, $id);

				return $stmt->execute();
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function actualizarItem($item, $tipo) {
			try {
				$this->conectar();
				$stmt = NULL;

				switch ($tipo) {
					case 'usuario':
						$stmt = $this->conn->prepare("UPDATE usuarios SET clave = ?, tipo = ?, estado = ? WHERE email LIKE ?");
						$stmt->bindParam(1, $item->getClave());
						$stmt->bindParam(2, $item->getTipo());
						$stmt->bindParam(3, $item->getEstado());
						$stmt->bindParam(4, $item->getEmail());
						break;
					case 'estilo':
						$stmt = $this->conn->prepare("UPDATE estilos SET nombre = ?, origen = ?, imagen = ? WHERE id = ?");
						$stmt->bindParam(1, $item->getNombre());
						$stmt->bindParam(2, $item->getOrigen());
						$stmt->bindParam(3, $item->getImagen());
						$stmt->bindParam(4, $item->getId());
						break;
					case 'instrumento':
						$stmt = $this->conn->prepare("UPDATE instrumentos SET nombre = ?, tipo = ?, origen = ?, imagen = ? WHERE id = ?");
						$stmt->bindParam(1, $item->getNombre());
						$stmt->bindParam(2, $item->getTipo());
						$stmt->bindParam(3, $item->getOrigen());
						$stmt->bindParam(4, $item->getImagen());
						$stmt->bindParam(5, $item->getId());
						break;
					case 'compositor':
						$stmt = $this->conn->prepare("UPDATE compositores SET nombre = ?, apellidos = ?, direccion = ?, imagen = ? WHERE id = ?");
						$stmt->bindParam(1, $item->getNombre());
						$stmt->bindParam(2, $item->getApellidos());
						$stmt->bindParam(3, $item->getDireccion());
						$stmt->bindParam(4, $item->getImagen());
						$stmt->bindParam(5, $item->getId());
						break;
				}

				return $stmt->execute();
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function actualizarPartitura($item) {
			try {
				$this->conectar();
				$stmt = NULL;

				$stmt = $this->conn->prepare("UPDATE partituras SET titulo = ?, anio = ?, id_compositor = ?, id_estilo = ?,
					pdf = ?, audio = ?, email_usuario = ? WHERE id = ?");

				$stmt->bindParam(1, $item->getTitulo());
				$stmt->bindParam(2, $item->getAnio());
				$stmt->bindParam(3, $item->getIdCompositor());
				$stmt->bindParam(4, $item->getIdEstilo());
				$stmt->bindParam(5, $item->getPdf());
				$stmt->bindParam(6, $item->getAudio());
				$stmt->bindParam(7, $item->getEmailUsuario());
				$stmt->bindParam(8, $item->getId());

				return $stmt->execute();
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

	 	
	    // Mètodes (Usuaris)
		public function validarUsuario($user) {
			try {
				if (!$this->existeItem($user, 'usuario')) return -1;
				$this->conectar();

				$stmt = $this->conn->prepare("SELECT * FROM usuarios WHERE email LIKE ? AND clave LIKE ?");
				$stmt->bindParam(1, $user->getEmail());
				$stmt->bindParam(2, $user->getClave());
				$stmt->execute();
				$row = $stmt->fetchObject();

				if (!$row) return 0;
				return 1;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		public function obtenerTipoUsuario($mail) {
			try {
				$this->conectar();

				$stmt = $this->conn->prepare("SELECT tipo FROM usuarios WHERE email LIKE ?");
				$stmt->bindParam(1, $mail);
				$stmt->execute();
				$row = $stmt->fetchObject();

				return $row->tipo;
			}
			catch(PDOException $e) {
			    echo "<br>" . $e->getMessage();
			}
			finally {
				$this->desconectar();
			}
		}

		// public function obtenerEstadoUsuario($mail) {
		// 	try {
		// 		$this->conectar();

		// 		$stmt = $this->conn->prepare("SELECT estado FROM usuarios WHERE email LIKE ?");
		// 		$stmt->bindParam(1, $mail);
		// 		$stmt->execute();
		// 		$row = $stmt->fetchObject();

		// 		return $row->estado;
		// 	}
		// 	catch(PDOException $e) {
		// 	    echo "<br>" . $e->getMessage();
		// 	}
		// 	finally {
		// 		$this->desconectar();
		// 	}
		// }

		// public function obtenerUsuario($correo) {
		// 	try {
		// 		$this->conectar();

		// 		$stmt = $this->conn->prepare("SELECT * from usuarios WHERE email LIKE ?");
		// 		$stmt->bindParam(1, $correo);
		// 		$stmt->execute();
		// 		$row = $stmt->fetchObject();
				
		// 		return $row;
		// 	}
		// 	catch(PDOException $e) {
		// 	    echo "<br>" . $e->getMessage();
		// 	}
		// 	finally {
		// 		$this->desconectar();
		// 	}
		// }
	}
?>

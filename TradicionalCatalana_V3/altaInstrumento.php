<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tradicional Catalana - Alta de instrumento</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/validaciones_ajax.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="altaItem">
	<div class="container-fluid">
		<!-- HEADER -->
		<?php require_once "templates/header_logged_adm.php"; ?>

		<!-- CONTINGUT -->
		<div class="row" id="content">
			<div class="col-md-4 thumbnail datos">
				<form method="POST" onsubmit="return altaInstrumento()" action="controller.php" enctype="multipart/form-data">
					<fieldset>
						<legend>Alta de Instrumento</legend>
						<label>Nombre: </label>
						<input type="text" id="nombre" name="nombre" placeholder="Nombre" required><br>
						<label>Tipo: </label>
						<select id="tipos" name="tipos">
						<option value="viento" selected>Viento</option>
						<option value="cuerda">Cuerda</option>
						<option value="percusion">Percusi&oacute;n</option>
						</select>
						<br>
						<label>Origen: </label>
						<select id="origenes" name="origenes">
						<option value="africa" selected>&Aacute;frica</option>
						<option value="america">Am&eacute;rica</option>
						<option value="asia">Asia</option>
						<option value="europa">Europa</option>
						<option value="oceania">Ocean&iacute;a</option>
						</select>
						<br>
						<label class="lblFichero">Imagen: </label>
						<input type="file" id="fotoInstrumento" name="fotoInstrumento" placeholder="Imagen"><br>
					</fieldset>
					<input type="submit" value="Alta">
					<div id="error"></div>
				</form>
			</div>			
		</div>

		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>		
	</div>
</body>
</html>

<?php
	session_start();
	require_once "models/GestorItems.php";
	require_once "models/Usuario.php";
	require_once "models/Partitura.php";
	require_once "models/Estilo.php";
	require_once "models/Instrumento.php";
	require_once "models/Compositor.php";

	switch (true) {
		// Login
		case isset($_POST['action']) && $_POST['action'] == 1:
			echo login($_POST['data']);
			break;

		// Alta de usuario
		case isset($_POST['action']) && $_POST['action'] == 2:
			echo altaUsuario($_POST['data']);
			break;

		// Alta de partitura
		case isset($_POST['action']) && $_POST['action'] == 3:
			echo altaPartitura($_POST['data']);
			break;

		// Alta de estilo
		case isset($_POST['action']) && $_POST['action'] == 4:
			echo altaEstilo($_POST['data']);
			break;

		// Alta de instrumento
		case isset($_POST['action']) && $_POST['action'] == 5:
			echo altaInstrumento($_POST['data']);
			break;

		// Alta de compositor
		case isset($_POST['action']) && $_POST['action'] == 6:
			echo altaCompositor($_POST['data']);
			break;

		// Baja de usuario
		case isset($_POST['action']) && $_POST['action'] == 7:
			echo bajaUsuario($_POST['data']);
			break;

		// Actualización de estilo
		case isset($_POST['action']) && $_POST['action'] == 8:
			echo actualizarEstilo($_POST['data']);
			break;

		// Actualización de instrumento
		case isset($_POST['action']) && $_POST['action'] == 9:
			echo actualizarInstrumento($_POST['data']);
			break;

		// Actualización de usuario
		case isset($_POST['action']) && $_POST['action'] == 10:
			echo actualizarUsuario($_POST['data']);
			break;

		// Actualización de compositor
		case isset($_POST['action']) && $_POST['action'] == 11:
			echo actualizarCompositor($_POST['data']);
			break;

		// Actualización de partitura
		case isset($_POST['action']) && $_POST['action'] == 12:
			echo actualizarPartitura($_POST['data']);
			break;

		// Borrado de partitura
		case isset($_POST['action']) && $_POST['action'] == 13:
			echo borrarPartitura($_POST['data']);
			break;

		// Borrado de estilo
		case isset($_POST['action']) && $_POST['action'] == 14:
			echo borrarEstilo($_POST['data']);
			break;

		// Borrado de instrumento
		case isset($_POST['action']) && $_POST['action'] == 15:
			echo borrarInstrumento($_POST['data']);
			break;

		// Borrado de compositor
		case isset($_POST['action']) && $_POST['action'] == 16:
			echo borrarCompositor($_POST['data']);
			break;

		// Subida de ficheros
		case isset($_FILES['fotoEstilo']):
			move_uploaded_file($_FILES["fotoEstilo"]["tmp_name"], "images/" . 'style_' . $_FILES['fotoEstilo']['name']);
			header('Location: index.php');
			break;

		case isset($_FILES['fotoCompositor']):
			move_uploaded_file($_FILES["fotoCompositor"]["tmp_name"], "images/" . 'comp_' . $_FILES['fotoCompositor']['name']);
			header('Location: index.php');
			break;

		case isset($_FILES['fotoInstrumento']):
			move_uploaded_file($_FILES["fotoInstrumento"]["tmp_name"], "images/" . 'inst_' . $_FILES['fotoInstrumento']['name']);
			header('Location: index.php');
			break;

		case isset($_FILES['pdf']) && isset($_FILES['audio']):
			move_uploaded_file($_FILES["pdf"]["tmp_name"], "pdfs/" . 'pdf_' . $_FILES['pdf']['name']);
			move_uploaded_file($_FILES["audio"]["tmp_name"], "audios/" . 'audio_' . $_FILES['audio']['name']);
			header('Location: index.php');
			break;
	}

	function login($arrayDatos)	{
		$datos = json_decode($arrayDatos);
		$user = new Usuario(strtolower(trim($datos[0])), strtolower(trim($datos[1])));
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->validarUsuario($user) == 1) {
			$_SESSION['usuario'] = $user->getEmail();
			$outputData[0]=true;
			$outputData[1]="El usuario y la clave son correctos.";
		}
		elseif ($gi->validarUsuario($user) == 0) {
			$outputData[0]=false;
			$outputData[1]="Error!!! La clave es incorrecta.";	
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! El usuario no existe.";
		}

		return json_encode($outputData);
	}

	function altaUsuario($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$user = new Usuario(strtolower(trim($datos[0])), strtolower(trim($datos[1])), "usr", 1);
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->existeItem($user, 'usuario')) {
			$outputData[0]=false;
			$outputData[1]="Error!!! El usuario ya existe";
		}
		else {
			$gi->agregarItem($user, 'usuario');
			$outputData[0]=true;
			$outputData[1]="El usuario se ha registrado correctamente.";
		}

		return json_encode($outputData);
	}

	function altaEstilo($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$estilo = new Estilo(strtolower(trim($datos[0])), strtolower(trim($datos[1])), strtolower(trim($datos[2])));
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->existeItem($estilo, 'estilo')) {
			$outputData[0]=false;
			$outputData[1]="Error!!! El estilo ya existe";
		}
		else {
			$gi->agregarItem($estilo, 'estilo');
			$outputData[0]=true;
			$outputData[1]="El estilo se ha registrado correctamente.";
		}

		return json_encode($outputData);
	}

	function altaInstrumento($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$instrumento = new Instrumento(strtolower(trim($datos[0])), strtolower(trim($datos[1])),
			strtolower(trim($datos[2])), strtolower(trim($datos[3])));
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->existeItem($instrumento, 'instrumento')) {
			$outputData[0]=false;
			$outputData[1]="Error!!! El instrumento ya existe";
		}
		else {
			$gi->agregarItem($instrumento, 'instrumento');
			$outputData[0]=true;
			$outputData[1]="El instrumento se ha registrado correctamente.";			
		}

		return json_encode($outputData);
	}

	function altaCompositor($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$compositor = new Compositor(strtolower(trim($datos[0])), strtolower(trim($datos[1])),
			strtolower(trim($datos[2])), strtolower(trim($datos[3])));
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->existeItem($compositor, 'compositor')) {
			$outputData[0]=false;
			$outputData[1]="Error!!! El compositor ya existe";
		}
		else {
			$gi->agregarItem($compositor, 'compositor');
			$outputData[0]=true;
			$outputData[1]="El compositor se ha registrado correctamente.";
		}

		return json_encode($outputData);
	}

	function altaPartitura($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$part = new Partitura(strtolower(trim($datos[0])), $datos[1], $datos[2], $datos[3],
			strtolower(trim($datos[5])), strtolower(trim($datos[6])), $_SESSION['usuario']);
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->existeItem($part, 'partitura')) {
			$outputData[0]=false;
			$outputData[1]="Error!!! La partitura ya existe";
		}
		else {
			$gi->agregarPartitura($part, $datos[4]);
			$outputData[0]=true;
			$outputData[1]="La partitura se ha registrado correctamente.";
		}

		return json_encode($outputData);
	}

	function bajaUsuario($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$user = new Usuario(strtolower(trim($datos[0])), strtolower(trim($datos[1])));
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->validarUsuario($user) == 1) {
			session_unset();
			session_destroy();
			$user->setEstado(0);
			$gi->borrarItem($user, 'usuario');
			$gi->asignarUsuarioPartituras($user->getEmail());
			$outputData[0]=true;
			$outputData[1]="El usuario y la clave son correctos.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! La clave es incorrecta.";	
		}

		return json_encode($outputData);
	}

	function actualizarEstilo($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$estilo = new Estilo($datos[0], strtolower(trim($datos[1])), strtolower(trim($datos[2])), strtolower(trim($datos[3])));
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->actualizarItem($estilo, 'estilo')) {
			$outputData[0]=true;
			$outputData[1]="El estilo se ha actualizado correctamente.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! El estilo no se ha podido actualizar";
		}

		return json_encode($outputData);
	}

	function actualizarInstrumento($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$instrum = new Instrumento($datos[0], strtolower(trim($datos[1])), strtolower(trim($datos[2])), strtolower(trim($datos[3])), strtolower(trim($datos[4])));
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->actualizarItem($instrum, 'instrumento')) {
			$outputData[0]=true;
			$outputData[1]="El instrumento se ha actualizado correctamente.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! El instrumento no se ha podido actualizar";
		}

		return json_encode($outputData);
	}

	function actualizarUsuario($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$user = new Usuario(strtolower(trim($datos[0])), strtolower(trim($datos[1])), strtolower(trim($datos[2])), $datos[3]);
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->actualizarItem($user, 'usuario')) {
			$outputData[0]=true;
			$outputData[1]="El usuario se ha actualizado correctamente.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! El usuario no se ha podido actualizar";
		}

		return json_encode($outputData);
	}

	function actualizarCompositor($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$comp = new Compositor($datos[0], strtolower(trim($datos[1])), strtolower(trim($datos[2])), strtolower(trim($datos[3])), strtolower(trim($datos[4])));
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->actualizarItem($comp, 'compositor')) {
			$outputData[0]=true;
			$outputData[1]="El compositor se ha actualizado correctamente.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! El compositor no se ha podido actualizar";
		}

		return json_encode($outputData);
	}

	function actualizarPartitura($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$part = new Partitura($datos[0], strtolower(trim($datos[1])), $datos[2], $datos[3], $datos[4],
			strtolower(trim($datos[5])), strtolower(trim($datos[6])), strtolower(trim($datos[7])));
		$gi = new GestorItems();
		$outputData = array();

		if ($gi->actualizarPartitura($part)) {
			$outputData[0]=true;
			$outputData[1]="La partitura se ha actualizado correctamente.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! La partitura no se ha podido actualizar";
		}

		return json_encode($outputData);
	}

	function borrarEstilo($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$estilo = new Estilo($datos[0]);
		$gi = new GestorItems();
		$auxItem = $gi->obtenerItemById($estilo->getId(), 'estilo');
		$outputData = array();

		if ($gi->borrarItem($estilo, 'estilo')) {
			unlink('images/' . $auxItem->imagen);
			$outputData[0]=true;
			$outputData[1]="El estilo se ha borrado correctamente.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! El estilo no se ha podido borrar.";
		}

		return json_encode($outputData);
	}

	function borrarCompositor($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$comp = new Compositor($datos[0]);
		$gi = new GestorItems();
		$auxItem = $gi->obtenerItemById($comp->getId(), 'compositor');
		$outputData = array();

		if ($gi->borrarItem($comp, 'compositor')) {
			unlink('images/' . $auxItem->imagen);
			$outputData[0]=true;
			$outputData[1]="El coompositor se ha borrado correctamente.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! El compositor no se ha podido borrar.";
		}

		return json_encode($outputData);
	}

	function borrarInstrumento($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$inst = new Instrumento($datos[0]);
		$gi = new GestorItems();
		$auxItem = $gi->obtenerItemById($inst->getId(), 'instrumento');
		$outputData = array();

		if ($gi->borrarItem($inst, 'instrumento')) {
			unlink('images/' . $auxItem->imagen);
			$outputData[0]=true;
			$outputData[1]="El instrumento se ha borrado correctamente.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! El instrumento no se ha podido borrar.";
		}

		return json_encode($outputData);
	}

	function borrarPartitura($arrayDatos) {
		$datos = json_decode($arrayDatos);
		$part = new Partitura($datos[0]);
		$gi = new GestorItems();
		$auxItem = $gi->obtenerItemById($part->getId(), 'partitura');
		$outputData = array();

		if ($gi->borrarPartitura($part->getId())) {
			unlink('pdfs/' . $auxItem->pdf);
			unlink('audios/' . $auxItem->audio);
			$outputData[0]=true;
			$outputData[1]="La partitura se ha borrado correctamente.";
		}
		else {
			$outputData[0]=false;
			$outputData[1]="Error!!! La partitura no se ha podido borrar.";
		}

		return json_encode($outputData);
	}
?>


<?php
	session_start();
	
	session_unset();
	session_destroy();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tradicional Catalana - Salida</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="salida">
	<div class="container-fluid">
		<!-- HEADER -->
		<?php require_once "templates/header_generic.php"; ?>

		<!-- CONTINGUT -->
		<div class="row" id="content">
		<?php
			$texto = 'Has cerrado la sesi&oacute;n. ;)';
			require "templates/aviso.php";
			header('Refresh: 2; url=index.php');
		?>
		</div>

		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>
	</div>
</body>
</html>

<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tradicional Catalana - Gesti&oacute;n de &iacute;tems</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/validaciones_ajax.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap-3.3.7.min.css">
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-3.3.7.min.js"></script> -->
	<!-- <script type="text/javascript" src="js/controller_index.js"></script> -->
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="index">
	<div class="container-fluid">
		<!-- HEADER -->
		<?php
			require_once "models/GestorItems.php";
			$tipoUsuario = "guest";

			if (isset($_SESSION['usuario'])) {
				$user = new Usuario($_SESSION['usuario']);
				$gi = new GestorItems();
				$tipoUsuario = $gi->obtenerTipoUsuario($user->getEmail());

				if ($tipoUsuario == "usr")
					require_once "templates/header_logged_usr.php";
				else
					require_once "templates/header_logged_adm.php";
			}
			else
				require_once "templates/header_generic.php";
		?>
		
		<!-- CONTINGUT -->
		<div class="row" id="content">
			<?php
				require_once "models/GestorItems.php";
				require_once "models/Utilidades.php";

				$gi = new GestorItems();
				$ut = new Utilidades();
				$items = NULL;

				if (isset($_POST['buscarItems'])) {
					$items = $gi->buscarItems(trim($_POST['criterio']), 'partitura');
					if (count($items) == 0)
						$items = $gi->obtenerItems('partitura', $_SESSION['usuario']);
					$ut->mostrarItems($items, 'partitura');
				}
				else {
					$items = $gi->obtenerItems('partitura', $_SESSION['usuario']);
					if (count($items) == 0) {
						$texto = 'Partituras';
						require "templates/titulo_categoria.php";
						$ut->mostrarAviso('partitura');
					}
					else
						$ut->mostrarItemsTabla($items, 'partitura', 'usr');
				}
			?>			
		</div>
		
		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>
	</div>
</body>
</html>

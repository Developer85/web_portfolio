<div class="col-xs-11 col-md-4 thumbnail item resultante text-right" id="item<?php echo $row->id ?>">
	<img src="images/score_icon.png" alt="score_icon.png">
	<h2><?php echo ucwords($row->titulo) ?></h2>
	<ul>
		<li><label>T&iacute;tulo:</label><?php echo ucwords($row->titulo) ?></li>
		<li><label>A&ntilde;o:</label><?php echo $row->anio ?></li>
		<li><label>Compositor:</label><?php echo ucwords($nomCompositor) ?></li>
		<li><label>Estilo:</label><?php echo ucwords($nomEstilo) ?></li>
		<li><label>Instrumentos:</label><?php
			for ($i=0; $i<count($instrumentos); $i++) {
				if ($i < count($instrumentos) -1)
					echo ucfirst($instrumentos[$i][0]) . ' / ';
				else
					echo ucfirst($instrumentos[$i][0]);
			}
		?></li>
		<li><label>Compartido por:</label><?php echo $row->email_usuario ?><br></li>
	</ul>
	<p id="descargas">
		<a class="btn btn-warning" href="pdfs/<?php echo $row->pdf ?>">PDF</a>
		<a class="btn btn-warning" href="audios/<?php echo $row->audio ?>">Audio</a>
	</p>
</div>

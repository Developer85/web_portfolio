<div class="col-xs-11 col-md-4 thumbnail item resultante text-right" id="item<?php echo $row->id ?>">
	<img src="images/<?php echo $row->imagen ?>" alt="<?php echo $row->imagen ?>">
	<h2><?php echo ucfirst($row->nombre) ?></h2>
	<ul>
		<li><label>Nombre:</label><?php echo ucfirst($row->nombre) ?></li>
		<li><label>Tipo:</label><?php echo ucfirst($row->tipo) ?></li>
		<li><label>Origen:</label><?php echo ucfirst($row->origen) ?></li>
	</ul>
</div>

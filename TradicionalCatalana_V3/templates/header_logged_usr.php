<div class="row" id="header">
	<div class="col-md-12">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navbar-header menu">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand not-active" href="#">TradicionalCatalana</a>
			</div>
			<div class="collapse navbar-collapse menu" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.php">Inicio</a></li>
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Categor&iacute;as <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="index.php?categ=partituras">Partituras</a></li>
							<li><a href="index.php?categ=estilos">Estilos</a></li>
							<li><a href="index.php?categ=instrumentos">Instrumentos</a></li>
							<li><a href="index.php?categ=compositores">Compositores</a></li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Partituras <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="altaPartitura.php">Insertar</a></li>
							<li><a href="gestionPartituras.php">Gestionar</a></li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo ucfirst(strtok($_SESSION['usuario'], '@'));?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="edicionPerfil.php">Editar perfil</a></li>
							<li><a href="bajaUsuario.php">Darse de baja</a></li>
							<li><a href="salida.php">Cerrar sesi&oacute;n</a></li>
						</ul>
					</li>
				</ul>
				<form class="navbar-form navbar-right" method="POST" action="">
					<input type="text" class="form-control" id="criterio" name="criterio" placeholder="Search">
					<button type="submit" class="btn btn-default search" name="buscarItems"><i class="glyphicon glyphicon-search"></i></button>
				</form>
			</div>
		</nav>
	</div>
</div>


<div class="col-xs-11 col-md-4 thumbnail item resultante text-right" id="item<?php echo $row->id ?>">
	<img src="images/<?php echo $row->imagen ?>" alt="<?php echo $row->imagen ?>">
	<h2><?php echo ucwords($row->nombre) ?></h2>
	<ul>
		<li><label>Nombre:</label><?php echo ucwords($row->nombre) ?></li>
		<li><label>Apellidos:</label><?php echo ucwords($row->apellidos) ?></li>
		<li><label>Direcci&oacute;n:</label><?php echo ucwords($row->direccion) ?></li>
	</ul>
</div>

<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tradicional Catalana - Ficha de &iacute;tem</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="fichaItem">
	<div class="container-fluid">		
		<!-- HEADER -->
		<?php
			require_once "models/GestorItems.php";
			$tipoUsuario = "guest";

			if (isset($_SESSION['usuario'])) {
				$user = new Usuario($_SESSION['usuario']);
				$gi = new GestorItems();
				$tipoUsuario = $gi->obtenerTipoUsuario($user->getEmail());

				if ($tipoUsuario == "usr")
					require_once "templates/header_logged_usr.php";
				else
					require_once "templates/header_logged_adm.php";
			}
			else
				require_once "templates/header_generic.php";
		?>
		
		<!-- CONTINGUT -->
		<div class="row" id="content">
			<?php
				require_once "models/GestorItems.php";
				require_once "models/Utilidades.php";

				$gi = new GestorItems();
				$ut = new Utilidades();
				$item = NULL;

				if (isset($_GET['partitura'])) {
					$item = $gi->obtenerItem($_GET['partitura'], 'partitura');
					$ut->mostrarItem($item, 'partitura');
				}
				elseif (isset($_GET['estilo'])) {
					$item = $gi->obtenerItem($_GET['estilo'], 'estilo');
					$ut->mostrarItem($item, 'estilo');
				}
				elseif (isset($_GET['instrumento'])) {
					$item = $gi->obtenerItem($_GET['instrumento'], 'instrumento');
					$ut->mostrarItem($item, 'instrumento');
				}
				elseif (isset($_GET['compositor'])) {
					$item = $gi->obtenerItem($_GET['compositor'], 'compositor');
					$ut->mostrarItem($item, 'compositor');
				}
			?>
		</div>
		
		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>		
	</div>
</body>
</html>

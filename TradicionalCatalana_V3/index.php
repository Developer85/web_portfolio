<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tradicional Catalana - Inicio</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/headerFooter_fix.js"></script>
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="index">
	<div class="container-fluid">
		<!-- HEADER -->
		<?php
			require_once "models/GestorItems.php";
			$tipoUsuario = "guest";

			if (isset($_SESSION['usuario'])) {
				$user = new Usuario($_SESSION['usuario']);
				$gi = new GestorItems();
				$tipoUsuario = $gi->obtenerTipoUsuario($user->getEmail());

				if ($tipoUsuario == "usr")
					require_once "templates/header_logged_usr.php";
				else
					require_once "templates/header_logged_adm.php";
			}
			else
				require_once "templates/header_generic.php";
		?>
		
		<!-- CONTINGUT -->
		<div class="row" id="content">
			<?php
				require_once "models/GestorItems.php";
				require_once "models/Utilidades.php";

				$gi = new GestorItems();
				$ut = new Utilidades();
				$items = NULL;

				if (isset($_POST['buscarItems'])) {
					if (isset($_GET['categ'])) {
						switch ($_GET['categ']) {
							case 'partituras':
								$items = $gi->buscarItems(trim($_POST['criterio']), 'partitura');
								if (count($items) == 0)
									$items = $gi->obtenerItems('partitura', $_SESSION['usuario']);
								$ut->mostrarItems($items, 'partitura');
								break;

							case 'estilos':
								$items = $gi->buscarItems(trim($_POST['criterio']), 'estilo');
								if (count($items) == 0)
									$items = $gi->obtenerItems('estilo', "");
								$ut->mostrarItems($items, 'estilo');
								break;

							case 'instrumentos':
								$items = $gi->buscarItems(trim($_POST['criterio']), 'instrumento');
								if (count($items) == 0)
									$items = $gi->obtenerItems('instrumento', "");
								$ut->mostrarItems($items, 'instrumento');
								break;

							case 'compositores':
								$items = $gi->buscarItems(trim($_POST['criterio']), 'compositor');
								if (count($items) == 0)
									$items = $gi->obtenerItems('compositor', "");
								$ut->mostrarItems($items, 'compositor');
								break;
						}
					}
					else {
						$ut->mostrarCategorias();
					}
				}
				else {
					if (isset($_GET['categ'])) {
						$texto = '';
						switch ($_GET['categ']) {
							case 'partituras':
								if (isset($_SESSION['usuario']))
									$items = $gi->obtenerItems('partitura', $_SESSION['usuario']);
								else
									$items = $gi->obtenerItems('partitura', "");
								if (count($items) > 0)
									$ut->mostrarItems($items, 'partitura');
								else {
									$texto = 'Partituras';
									require "templates/titulo_categoria.php";
									$ut->mostrarAviso('partitura');
								}
								break;
							case 'estilos':
								$items = $gi->obtenerItems('estilo', "");
								if (count($items) > 0)
									$ut->mostrarItems($items, 'estilo');
								else{
									$texto = 'Estilos';
									require "templates/titulo_categoria.php";
									$ut->mostrarAviso('estilo');
								}
								break;
							case 'instrumentos':
								$items = $gi->obtenerItems('instrumento', "");
								if (count($items) > 0)
									$ut->mostrarItems($items, 'instrumento');
								else {
									$texto = 'Instrumentos';
									require "templates/titulo_categoria.php";
									$ut->mostrarAviso('instrumento');
								}
								break;
							case 'compositores':
								$items = $gi->obtenerItems('compositor', "");
								if (count($items) > 0)
									$ut->mostrarItems($items, 'compositor');
								else {
									$texto = 'Compositores';
									require "templates/titulo_categoria.php";
									$ut->mostrarAviso('compositor');
								}
								break;
						}
					}
					else {
						$ut->mostrarCategorias();
					}
				}
			?>			
		</div>
		
		<!-- FOOTER -->
		<?php require_once "templates/footer.php"; ?>
	</div>
</body>
</html>

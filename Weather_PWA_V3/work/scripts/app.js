// Copyright 2016 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

(function() {
  'use strict';

  // TODO Autocomplete the name of a found city on Yahoo Weather
  window.autocompletar = function() {
    var txt = document.getElementById('nameCity').value.toLowerCase();
  }

  // Add elements to index.html
  function fillInitialHtml() {
    var main = document.getElementsByClassName("main")[0];

    // Information dialog
    const infoDialog = `
      <div class="dialog-container">
        <div class="dialog">
          <div class="dialog-title"></div>
          <div class="dialog-body"></div>
          <div class="dialog-buttons">
            <button id="butInfoAccept" class="button">Close</button>
          </div>
        </div>
      </div>
    `;

    main.insertAdjacentHTML('afterend', infoDialog);

    // Remove city dialog
    const removeCityDialog = `
      <div class="dialog-container">
        <div class="dialog">
          <div class="dialog-title">Remove a city</div>
          <div class="dialog-body">
            <select id="selectCityToRemove">
            </select>
          </div>
          <div class="dialog-buttons">
            <button id="butRemoveCity" class="button">Remove</button>
            <button id="butRemoveCancel" class="button">Cancel</button>
          </div>
        </div>
      </div>
    `;

    main.insertAdjacentHTML('afterend', removeCityDialog);

    // Add city dialog
    const addCityDialog = `
      <div class="dialog-container">
        <div class="dialog">
          <div class="dialog-title">Add a city</div>
          <div class="dialog-body">
              <label>City: </label>
              <input type="text" id="nameCity" placeholder="City" onkeyup="autocompletar()"><br>
          </div>
          <div class="dialog-buttons">
            <button id="butAddCity" class="button">Add</button>
            <button id="butAddCancel" class="button">Cancel</button>
          </div>
        </div>
      </div>
    `;

    main.insertAdjacentHTML('afterend', addCityDialog);    

    // Logo/Loader
    const loader = `
      <div class="loader">
        <img src="images/icons/icon-128x128.png" alt="Weather Logo" height="128" width="128">
      </div>
    `;

    main.insertAdjacentHTML('afterend', loader);
  };

  fillInitialHtml();

  var dc = document.querySelectorAll(".dialog-container");
  var app = {
    // Array of added cities
    selectedCities: [],
    // Logo or Loader
    loader: document.querySelector('.loader'),
    // Add city dialog
    addDialog: dc[0],
    // Remove city dialog
    removeDialog: dc[1],
    // Information dialog
    infoDialog: dc[2],
    // Array of days
    daysOfWeek: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  };

  // Fill the list of cities to remove
  app.fillRemovableCitiesList = function() {
    var cities = document.getElementById("selectCityToRemove");
    var option;

    // Remove all the 'options' of the 'select'
    while (cities.options.length > 0) {                
      cities.remove(0);
    }

    // Fill the 'select' with the cities saved in LocalStorage
    app.selectedCities = localStorage.selectedCities;
    if (app.selectedCities) {
      app.selectedCities = JSON.parse(app.selectedCities);
      app.selectedCities.forEach(function(city) {
        option = `
          <option value="${city.codeCity}">${city.nameCity}</option>
        `;
        cities.insertAdjacentHTML('beforeend', option);
      });
    }
  };

  // Fill the info dialog with the desired message
  app.fillInfoDialog = function(msg) {
    const content = `
      <h2>${msg}</h2>
    `;
    app.infoDialog.getElementsByClassName('dialog-body')[0].insertAdjacentHTML('afterbegin', content);
  };

  // Get the WOEID of a city
  app.getWoeid = function(link) {
    var start = link.indexOf("city");
    var end = link.length -1;

    return link.slice(start + 5, end);
  };

  // Return the time converted to 24-hour format
  app.convertTime = function(t) {
    // Get your time (using a hard-coded year for parsing purposes)
    var time = new Date("0001-01-01 " + t);

    // Output your formatted version (using your DateTime)
    return time.getHours() + ':' + ('0' + time.getMinutes()).slice(-2);
  };

  // Remove the forecast of a selected city and delete info from LocalStorage
  app.removeCity = function() {
    var select = document.getElementById('selectCityToRemove');
    var selected = select.options[select.selectedIndex];
    var codeCity = selected.value;
    var nameCity = selected.textContent;
    
    // Obtain the index of the selection in 'selectedCities'
    var index = app.selectedCities.findIndex(function(e) {
      return e.codeCity == codeCity;
    });
    
    // Remove the selected index from 'selectedCities'
    app.selectedCities.splice(index, 1);
    
    // Save again to LocalStorage
    app.saveSelectedCities();

    // Remove the corresponding div of the city
    var elem = document.getElementById('fc' + nameCity);
    elem.parentNode.removeChild(elem);
  };

  // Tell if a selected city index exists before adding it
  app.existsCityToAdd = function(city) {
    // Obtain the index of the selection in 'selectedCities'
    var index = app.selectedCities.findIndex(function(e) {
      return e.nameCity == city;
    });
    
    if (index == -1)
      return false;
    return true;
  };

  // Save the selected cities to LocalStorage
  app.saveSelectedCities = function() {
    var selectedCities = JSON.stringify(app.selectedCities);
    localStorage.selectedCities = selectedCities;
  };

  // Add or update forecast cards
  app.updateForecastCard = function(data) {
    var main = document.getElementsByClassName("main")[0];
    var elem = document.getElementById('fc' + data.nameCity);
    var dataCreation = data.created;
    var sunrise = data.channel.astronomy.sunrise;
    var sunset = data.channel.astronomy.sunset;
    var current = data.channel.item.condition;
    var humidity = data.channel.atmosphere.humidity;
    var wind = data.channel.wind;
    var icon = app.getIconClass(current.code);
    var today = new Date();
    var daily;

    today = today.getDay();

    if (!elem) {
      // Add a new forecast card
      var card = `
        <div class="card weather-forecast" id="fc${data.nameCity}">
          <div class="city-key" hidden>${data.codeCity}</div>
          <div class="card-last-updated" hidden>${data.created}</div>
          <div class="location">${data.nameCity}</div>
          <div class="date">${current.date}</div>
          <div class="description">${current.text}</div>
          <div class="current">
            <div class="visual">
              <div class="icon ${icon}"></div>
              <div class="temperature">
                <span class="value">${current.temp}</span><span class="scale">°${data.channel.units.temperature}</span>
              </div>
            </div>
            <div class="description">
              <div class="humidity">${humidity}%</div>
              <div class="wind">
                <span class="value">${Math.round(wind.speed)}</span>
                <span class="scale">${data.channel.units.speed}</span>
                <span class="direction">${wind.direction}</span>°
              </div>
              <div class="sunrise">${app.convertTime(sunrise)}</div>
              <div class="sunset">${app.convertTime(sunset)}</div>
            </div>
          </div>
          <div class="future">
      `;

      for (var i = 0; i < 7; i++) {
        daily = data.channel.item.forecast[i];
        icon = app.getIconClass(daily.code);
        card += `
          <div class="oneday" id="fc${i}">
            <div class="date">${app.daysOfWeek[(i + today) % 7]}</div>
            <div class="icon ${icon}"></div>
            <div class="temp-high">
              <span class="value">${daily.high}</span>°
            </div>
            <div class="temp-low">
              <span class="value">${daily.low}</span>°
            </div>
          </div>
        `;
      }

      card += `
          </div>
        </div>
      `;
      
      main.insertAdjacentHTML('beforeend', card);
    } else {
      // Update an existing forecast card
      elem.querySelector('.card-last-updated').textContent = data.created;
      elem.querySelector('.date').textContent = current.date;
      elem.querySelector('.description').textContent = current.text;

      var visualClasses = elem.querySelector('.visual > div:first-child').classList;
      elem.querySelector('.visual > div:first-child').classList.remove(visualClasses.item(visualClasses.length -1));
      elem.querySelector('.visual > div:first-child').classList.add(icon);
      
      elem.querySelector('.current .temperature .value').textContent = current.temp;
      elem.querySelector('.current .humidity').textContent = humidity + '%';
      elem.querySelector('.current .wind .value').textContent = Math.round(wind.speed);
      elem.querySelector('.current .wind .direction').textContent = wind.direction;
      elem.querySelector('.current .sunrise').textContent = app.convertTime(sunrise);
      elem.querySelector('.current .sunset').textContent = app.convertTime(sunset);

      var fc, iconClasses;

      for (var i = 0; i < 7; i++) {
        daily = data.channel.item.forecast[i];
        icon = app.getIconClass(daily.code);
        fc = document.getElementById('fc' + i);

        fc.querySelector('.date').textContent = app.daysOfWeek[(i + today) % 7];

        iconClasses = fc.querySelector('div:nth-child(2)').classList;
        fc.querySelector('div:nth-child(2)').classList.remove(iconClasses.item(iconClasses.length -1));
        fc.querySelector('div:nth-child(2)').classList.add(icon);
        
        fc.querySelector('.temp-high .value').textContent = daily.high;
        fc.querySelector('.temp-low .value').textContent = daily.low;
      }
    }
  };

  // Gets a forecast for a specific city and updates the card with the data
  app.getForecast = function(codeCity, nameCity) {
    var statement = 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"' +
      nameCity + '\") and u=\'c\'';
    var url = 'https://query.yahooapis.com/v1/public/yql?format=json&q=' + statement;

    // Update the forecast card with the data from the cache
    if ('caches' in window) {
      caches.match(url).then(function(response) {
        if (response) {
          response.json().then(function updateFromCache(json) {
            var res = json.query.results;
            var woeid = app.getWoeid(res.channel.link);
            // If don't exist the city to LocalStorage, add it
            if (!app.existsCityToAdd(nameCity)) {
              app.selectedCities.push({codeCity: woeid, nameCity: res.channel.location.city});
              app.saveSelectedCities();
            }
            res.codeCity = codeCity;
            res.nameCity = nameCity;
            res.created = json.query.created;
            app.updateForecastCard(res);
          });
        }
      });
    }

    // Update the forecast card with the data from the server
    fetch(url).then(function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' + response.status);  
        return;  
      }

      response.json().then(function(data) {
        var res = data.query.results;
        if (res) {
          var woeid = app.getWoeid(res.channel.link);
          if (!app.existsCityToAdd(nameCity)) {
            app.selectedCities.push({codeCity: woeid, nameCity: res.channel.location.city});
            app.saveSelectedCities();
          }
          res.codeCity = woeid;
          res.nameCity = nameCity;
          res.created = data.query.created;
          app.updateForecastCard(res);
        } else {
          app.fillInfoDialog('City not found');
          app.toggleInfoDialog(true);
        }
      });
    }).catch(function(err) {  
      console.log('Fetch Error :-S', err);
    });
  };

  // Iterate all of the cards and attempt to get the latest forecast data
  app.updateForecasts = function() {
    app.selectedCities.forEach(function(city) {
      app.getForecast(city.codeCity, city.nameCity);
    });
  };

  // Get the icon class according to the a weather code
  app.getIconClass = function(weatherCode) {
    // Weather codes: https://developer.yahoo.com/weather/documentation.html#codes
    weatherCode = parseInt(weatherCode);
    switch (weatherCode) {
      case 25: // cold
      case 32: // sunny
      case 33: // fair (night)
      case 34: // fair (day)
      case 36: // hot
      case 3200: // not available
        return 'clear-day';
      case 0: // tornado
      case 1: // tropical storm
      case 2: // hurricane
      case 6: // mixed rain and sleet
      case 8: // freezing drizzle
      case 9: // drizzle
      case 10: // freezing rain
      case 11: // showers
      case 12: // showers
      case 17: // hail
      case 35: // mixed rain and hail
      case 40: // scattered showers
        return 'rain';
      case 3: // severe thunderstorms
      case 4: // thunderstorms
      case 37: // isolated thunderstorms
      case 38: // scattered thunderstorms
      case 39: // scattered thunderstorms (not a typo)
      case 45: // thundershowers
      case 47: // isolated thundershowers
        return 'thunderstorms';
      case 5: // mixed rain and snow
      case 7: // mixed snow and sleet
      case 13: // snow flurries
      case 14: // light snow showers
      case 16: // snow
      case 18: // sleet
      case 41: // heavy snow
      case 42: // scattered snow showers
      case 43: // heavy snow
      case 46: // snow showers
        return 'snow';
      case 15: // blowing snow
      case 19: // dust
      case 20: // foggy
      case 21: // haze
      case 22: // smoky
        return 'fog';
      case 24: // windy
      case 23: // blustery
        return 'windy';
      case 26: // cloudy
      case 27: // mostly cloudy (night)
      case 28: // mostly cloudy (day)
      case 31: // clear (night)
        return 'cloudy';
      case 29: // partly cloudy (night)
      case 30: // partly cloudy (day)
      case 44: // partly cloudy
        return 'partly-cloudy-day';
    }
  };

  // Toggles the visibility of the add new city dialog
  app.toggleAddDialog = function(visible) {
    if (visible) {
      app.addDialog.classList.add('dialog-container--visible');
    } else {
      app.addDialog.classList.remove('dialog-container--visible');
    }
  };

  // Toggles the visibility of the remove city dialog
  app.toggleRemoveDialog = function(visible) {
    if (visible) {
      app.removeDialog.classList.add('dialog-container--visible');
    } else {
      app.removeDialog.classList.remove('dialog-container--visible');
    }
  };

  // Toggles the visibility of the information dialog
  app.toggleInfoDialog = function(visible) {
    if (visible) {
      app.infoDialog.classList.add('dialog-container--visible');
    } else {
      app.infoDialog.classList.remove('dialog-container--visible');
    }
  };

  // Event listeners for the button 'Refresh'
  document.getElementById('butRefresh').addEventListener('click', function() {
    if (app.selectedCities.length > 0)
      app.updateForecasts();
  });

  // Event listeners for the button 'Add'
  document.getElementById('butAdd').addEventListener('click', function() {
    app.loader.setAttribute('hidden', true);
    document.getElementById('nameCity').value = "";
    app.toggleAddDialog(true);
  });

  document.getElementById('butAddCity').addEventListener('click', function() {
    var city = document.getElementById('nameCity').value;
    
    if (city == "") {
      app.fillInfoDialog('Empty field');
      app.toggleInfoDialog(true);
      // alert('Empty field.');
    } else if (app.existsCityToAdd(city)) {
      app.fillInfoDialog('City already exists');
      app.toggleInfoDialog(true);
      // alert('City already exists.');
    } else
      app.getForecast('', city);

    app.toggleAddDialog(false);

    if (app.selectedCities.length == 0)
      app.loader.removeAttribute('hidden');
  });

  document.getElementById('butAddCancel').addEventListener('click', function() {
    app.toggleAddDialog(false);
    
    if (app.selectedCities.length == 0)
      app.loader.removeAttribute('hidden');
  });

  // Event listeners for the button 'Remove'
  document.getElementById('butRemove').addEventListener('click', function() {
     if (app.selectedCities.length > 0) {
      app.loader.setAttribute('hidden', true);
      app.fillRemovableCitiesList();
      app.toggleRemoveDialog(true);
     }
  });

  document.getElementById('butRemoveCity').addEventListener('click', function() {
    app.removeCity();    
    app.toggleRemoveDialog(false);

    if (app.selectedCities.length == 0)
      app.loader.removeAttribute('hidden');
  });

  document.getElementById('butRemoveCancel').addEventListener('click', function() {
    app.toggleRemoveDialog(false);
  });

  // Event listeners for the button of the information dialog
  document.getElementById('butInfoAccept').addEventListener('click', function() {
    app.toggleInfoDialog(false);
  });

  // Checks if there are any cities saved in local storage. If so, then it parses the local storage data
  // and then displays a forecast card for each of the saved cities
  app.selectedCities = localStorage.selectedCities;
  if (app.selectedCities) {
    app.selectedCities = JSON.parse(app.selectedCities);

    if (app.selectedCities.length > 0) {
      app.selectedCities.forEach(function(city) {
        app.getForecast(city.codeCity, city.nameCity);
      });
      app.loader.setAttribute('hidden', true);
    }    
  } else {
    app.selectedCities = [];
    app.saveSelectedCities();
  }

  // Service Worker
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
      .register('./service-worker.js')
      .then(function() { console.log('Service Worker Registered'); });
  }
})();

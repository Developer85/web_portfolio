/*
* @author: Gabriel Cerdà
* @version: 23.0
* @description: this script controls all functions in Prova_20.html
* @date: 2017/03/30
*
*/

// Variables globals (dia, mes i any inicials del DatePicker)
var diaActual, mesActual, anioActual;

/*
* @name: ready
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que amaga el formulari quan es carrega la pàgina
* @date: 2017/03/30
* @params: function()
* @return: none
*
*/
$(document).ready(function() {
    $("#main").hide();
});		

/*
* @name: pedirDatos
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que demana les dades personals via prompt
* @date: 2017/03/30
* @params: none
* @return: none
*
*/
function pedirDatos() {
	var titulo = pedirTxt("Título", "Mi CV");
	var nombre = pedirTxt("Nombre", "Gabriel");
	var primApellido = pedirTxt("Primer Apellido", "Cerdà");				
	var segApellido = pedirTxt("Segundo Apellido", "Sancho");
	var dni = pedirDni("40999189X");
	var direccion = pedirTxt("Dirección", "La Mía");
	var telefono = pedirNumTelef("699588855");

	rellenarCampos(titulo, nombre, primApellido, segApellido, dni, direccion, telefono);	
	$("#btn_enterCV").hide();
	$("#main").show();
}

/*
* @name: rellenarCampos
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que omple els input amb les dades introduïdes al prompt
* @date: 2017/03/30
* @params: titulo, nombre, primApellido, segApellido, dni, direccion, telefono
* @return: none
*
*/
function rellenarCampos(titulo, nombre, primApellido, segApellido, dni, direccion, telefono) {
	document.getElementById("titulo").innerHTML = titulo;
	document.getElementById("nombre").value = nombre;
	document.getElementById("apellido1").value = primApellido;
	document.getElementById("apellido2").value = segApellido;
	document.getElementById("dni").value = dni.toUpperCase();
	document.getElementById("direccion").value = direccion;
	document.getElementById("numTelef").value = telefono;
}

/*
* @name: function
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció anònima que carrega el DatePicker a l'input i assigna un valor a les variables globals
* @date: 2017/03/30
* @params: none
* @return: none
*
*/
$(function() {
	$("#fechaNac").datepicker().datepicker("setDate", new Date());
	diaActual = $("#fechaNac").datepicker('getDate').getDate();                 
    mesActual = $("#fechaNac").datepicker('getDate').getMonth() + 1;             
    anioActual = $("#fechaNac").datepicker('getDate').getFullYear();
});

/*
* @name: preguntar
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que demana què es vol afegir (experiència, idioma, hobbies, links) i, en cas afirmatiu, es procedeix
* @date: 2017/03/30
* @params: elemento
* @return: none
*
*/
function preguntar(elemento) {
	var resp = confirm("Quieres añadir " + elemento + "?");

	if (resp) {
		var cantidad = pedirCantidad();
		crearRegistros(elemento, cantidad);
	}
}

/*
* @name: pedirCantidad
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que demana la quantitat d'elements que volem afegir
* @date: 2017/03/30
* @params: none
* @return: total
*
*/
function pedirCantidad() {
	var ok = false;
	var total;

	do {
		try {
			total = parseInt(prompt("Cuántos registros quieres añadir?"));
			if (isNaN(total) || total <= 0)
				throw "Cantidad INCORRECTA.";
			ok = true;
		} catch(err) {
			console.log(err);
		}
	} while (!ok);
	
	return total;
}

// 
/*
* @name: crearRegistros
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que crea la quantitat de registres que volem afegir del tipus indicat
* @date: 2017/03/30
* @params: elemento, cantidad
* @return: none
*
*/
function crearRegistros(elemento, cantidad) {
	var formContent, functionContent;
	
	switch (elemento) {
		case "Experiencia":
			$("#experience input[type='button']").hide();
			formContent = "<fieldset><legend>Experience</legend>";
			for (var i=1; i<= cantidad; i++) {
				functionContent = "this,\"msgExp" + i + "\",\"Pos and Company fields field must be filled.\"";
				formContent += "<input type='text' id='expPos" + i + "' class='incorrect' placeholder='Pos' ";
				formContent += "onkeyup='validarTxtForm(" + functionContent + ")'>";
				formContent += "<input type='text' id='expCompany" + i + "' class='incorrect' placeholder='Company' ";
				formContent += "onkeyup='validarTxtForm(" + functionContent + ")'>";
				formContent += "<select id='expYears" + i + "'>";
				formContent += "<option selected value='lessOneYear'>Less 1 year</option>";
				formContent += "<option value='oneYear'>1 year</option>";
				formContent += "<option value='moreOneYear'>More 1 year</option>";
				formContent += "</select>";
				formContent += "<span id='msgExp" + i + "'>Pos and Company fields must be filled.</span>";
				if (i < cantidad) formContent += "<hr>";
			}			
			formContent += "</fieldset>";
			$("#experience").html(formContent);
			break;
			
		case "Idiomas":
			$("#languages input[type='button']").hide();
			formContent = "<fieldset><legend>Languages</legend>";
			for (var i=1; i<= cantidad; i++) {
				functionContent = "this,\"msgLang" + i + "\",\"Language field must be filled.\"";
				formContent += "<input type='text' id='lang" + i + "' class='incorrect' placeholder='Language' ";
				formContent += "onkeyup='validarTxtForm(" + functionContent + ")'>";
				formContent += "<select id='langLevel" + i + "'>";
				formContent += "<option selected value='low'>Low</option>";
				formContent += "<option value='medium'>Medium</option>";
				formContent += "<option value='high'>High</option>";
				formContent += "</select>";
				formContent += "<span id='msgLang" + i + "'>Language field must be filled.</span>";
				if (i < cantidad) formContent += "<hr>";
			}
			formContent += "</fieldset>";
			$("#languages").html(formContent);
			break;
			
		case "Hobbies":
			$("#hobbies input[type='button']").hide();
			formContent = "<fieldset><legend>Hobbies</legend>";
			for (var i=1; i<= cantidad; i++) {
				functionContent = "this,\"msgHobb" + i + "\",\"Hobbie field must be filled.\"";
				formContent += "<input type='text' id='hobbie" + i + "' class='incorrect' placeholder='Hobbie' ";
				formContent += "onkeyup='validarTxtForm(" + functionContent + ")'>";
				formContent += "<span id='msgHobb" + i + "'>Hobbie field must be filled.</span>";
				if (i < cantidad) formContent += "<hr>";
			}
			formContent += "</fieldset>";
			$("#hobbies").html(formContent);
			break;
			
		case "Enlaces":
			$("#intLinks input[type='button']").hide();
			var link;
			formContent = "<fieldset><legend>Interest Links</legend>";
			for (var i=1; i<= cantidad; i++) {
				link = pedirTxt("Link " + i);
				formContent += "<a href='http://" + link + "' target='_blank' id='link" + i + "'>" + link + "</a>";
				if (i < cantidad) formContent += "<hr>";
			}
			formContent += "</fieldset>";
			$("#intLinks").html(formContent);
			break;
	}
}


// Funcions de Petició (Prompt)
/*
* @name: pedirTxt
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que demana un camp de tipus text
* @date: 2017/03/30
* @params: dato, porDefecto
* @return: contenido
*
*/
function pedirTxt(dato, porDefecto) {
	var ok = false;
	var contenido;

	do {
		try {
			contenido = prompt(dato + ":", porDefecto);
			if (!validarTxt(contenido))
				throw "Formato de " + dato + " INCORRECTO.";
			ok = true;
		} catch(err) {
			console.log(err);
		}
	} while (!ok);
	
	return contenido;
}

/*
* @name: pedirNumTelef
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que demana el núm. de telèfon
* @date: 2017/03/30
* @params: porDefecto
* @return: contenido
*
*/
function pedirNumTelef(porDefecto) {
	var ok = false;
	var contenido;

	do {
		try {
			contenido = prompt("Teléfono:", porDefecto);
			if (!validarTelef(contenido))
				throw "Formato de Teléfono INCORRECTO.";
			ok = true;
		} catch(err) {
			console.log(err);
		}
	} while (!ok);
	
	return contenido;
}

/*
* @name: pedirDni
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que demana el DNI
* @date: 2017/03/30
* @params: porDefecto
* @return: contenido
*
*/
function pedirDni(porDefecto) {
	var ok = false;
	var contenido;

	do {
		try {
			contenido = prompt("DNI:", porDefecto);
			if (!validarDni(contenido))
				throw "Formato de DNI INCORRECTO.";
			ok = true;
		} catch(err) {
			console.log(err);
		}
	} while (!ok);
	
	return contenido;
}


// Funcions de Validació (Globals)
/*
* @name: validarTxt
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que valida un camp de tipus text
* @date: 2017/03/30
* @params: contenido
* @return: true or false
*
*/
function validarTxt(contenido) {
	if (!isNaN(contenido) || contenido.length < 3)
		return false;
	return true;
}

/*
* @name: validarTelef
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que valida el núm. de telèfon
* @date: 2017/03/30
* @params: contenido
* @return: true or false
*
*/
function validarTelef(contenido) {
	if (isNaN(contenido) || contenido.length != 9 || contenido < 600000000 || contenido > 699999999)
		return false;
	return true;
}

/*
* @name: validarDni
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que valida el DNI
* @date: 2017/03/30
* @params: contenido
* @return: true or false
*
*/
function validarDni(contenido) {
	var num = contenido.substring(0, 8);
	var letra = contenido.substring(contenido.length - 1).toUpperCase();
	
	if (!isNaN(num) && num.length == 8 && letra == obtenerLetraDni(num) && contenido.length == 9)
		return true
	return false;
}

/*
* @name: obtenerLetraDni
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que retorna la lletra corresponent al DNI
* @date: 2017/03/30
* @params: dni
* @return: string
*
*/
function obtenerLetraDni(dni) {
	var letras = "TRWAGMYFPDXBNJZSQVHLCKET";
	var pos = dni % 23;
	
	return letras.substring(pos, pos + 1);
}


// Funcions de Validació (Formularis)
/*
* @name: validarTxtForm
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que valida un camp de tipus text
* @date: 2017/03/30
* @params: elemento, idSpan, mensaje
* @return: none
*
*/
function validarTxtForm(elemento, idSpan, mensaje) {
 	if (validarTxt(elemento.value)) {
		document.getElementById(elemento.getAttribute('id')).className = "correct";
		document.getElementById(idSpan).innerHTML = "";
	} else {
		document.getElementById(elemento.getAttribute('id')).className = "incorrect";
		document.getElementById(idSpan).innerHTML = mensaje;
	}
}

/*
* @name: validarDniForm
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que valida el DNI
* @date: 2017/03/30
* @params: elemento, idSpan, mensaje
* @return: none
*
*/
function validarDniForm(elemento, idSpan, mensaje) {
	if (validarDni(elemento.value)) {
		document.getElementById(elemento.getAttribute('id')).className = "correct";
		document.getElementById(idSpan).innerHTML = "";
	} else {
		document.getElementById(elemento.getAttribute('id')).className = "incorrect";
		document.getElementById(idSpan).innerHTML = mensaje;
	}
}

/*
* @name: validarTelefForm
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que valida el núm. de telèfon
* @date: 2017/03/30
* @params: elemento, idSpan, mensaje
* @return: none
*
*/
function validarTelefForm(elemento, idSpan, mensaje) {
	if (validarTelef(elemento.value)) {
		document.getElementById(elemento.getAttribute('id')).className = "correct";
		document.getElementById(idSpan).innerHTML = "";
	} else {
		document.getElementById(elemento.getAttribute('id')).className = "incorrect";
		document.getElementById(idSpan).innerHTML = mensaje;
	}
}

/*
* @name: validarFechaForm
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que valida la data de naixement
* @date: 2017/03/30
* @params: idSpan, mensaje
* @return: none
*
*/
function validarFechaForm(idSpan, mensaje) {
	var daySelect = $("#fechaNac").datepicker('getDate').getDate();                 
    var monthSelect = $("#fechaNac").datepicker('getDate').getMonth() + 1;             
    var yearSelect = $("#fechaNac").datepicker('getDate').getFullYear();
 
	if (daySelect > diaActual || monthSelect > mesActual || yearSelect > anioActual) {
		document.getElementById("fechaNac").className = "incorrect";
	 	document.getElementById(idSpan).innerHTML = mensaje;
	} else {
		document.getElementById("fechaNac").className = "correct";
	 	document.getElementById(idSpan).innerHTML = "";
	}
}

/*
* @name: validarCV
* @author: Gabriel Cerdà
* @version: 1.0
* @description: funció que comprova el total d'elements incorrectes i de links per poder mostrar o no la info personal
* @date: 2017/03/30
* @params: none
* @return: none
*
*/
function validarCV() {
	var numIncorrectos = $(".incorrect").length;
	var numLinks = $("#intLinks a").length;

	if (numIncorrectos == 0 && numLinks > 0) {
		var info = "DATOS PERSONALES";
		info += "\n\nNombre --> " + $("#nombre").val();
		info += "\nApellido1 --> " + $("#apellido1").val();
		info += "\nApellido2 --> " + $("#apellido2").val();
		info += "\nDNI --> " + $("#dni").val();
		info += "\nDirección --> " + $("#direccion").val();
		info += "\nTeléfono --> " + $("#numTelef").val();
		info += "\nFecha Nacimiento --> " + $("#fechaNac").val();
		info += "\nEstado --> " + $("#marStatus option:selected").text();
		alert(info);
	}
	else
		alert("Queda algún campo por comprobar.");
}

